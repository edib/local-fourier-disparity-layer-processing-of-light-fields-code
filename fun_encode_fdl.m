function LFRec = fun_encode_fdl(resuDir,LFName,LFRef,uRange,vRange,crop,...
    Order,SubOrder,QPList,FFTParams,numLayers,lambdaCalib,lambdaConstruct,LFLab)
%ENCODEFDL Summary of this function goes here
%   Detailed explanation goes here

tStart = tic;

%% Initialize parameters
uRange = -1:2/(numel(uRange)-1):1;
vRange = -1:2/(numel(vRange)-1):1;

[V_ref,U_ref] = ndgrid(vRange,uRange);
U_ref = U_ref(:);
V_ref = V_ref(:);

D_ref = zeros(1,numLayers);

sz = size(LFRef);
imgSize = sz(1:2);
nChan = sz(3);
angSize = sz(4:5);

numPix = repmat(prod(imgSize),angSize);
numPix_All = prod(imgSize)*prod(angSize);

ConfigFile = fullfile(HEVC.Dir,'cfg','encoder_randomaccess_main_rext.cfg');
HEVCParams.ConfigFile = ConfigFile;
HEVCParams.subSamp = '444';
HEVCParams.fullRange = true;

if ~exist('LFLab','var')
    LFLab = [];
end

local = false;
if ~isempty(LFLab)
    local = true;
    
    % Avoid empty labels
    [~,~,LFLab(:)] = unique(LFLab);
    
    % Compute super-rays
    LFDims = arrayfun(@(x) 1:x,sz,'UniformOutput',false);
    numlab = max(LFLab(:));
    
    SRDims = cell(1,numlab);
    SRMask = cell(1,numlab);
    SRRef = cell(1,numlab);
    SRRec = cell(1,numlab);
    rMod = cell(1,numlab);
    scale = cell(1,numlab);    
    
    
    for lab = 1:numlab
        %Compute super-ray extent from labels
        [~,~,SRDims{lab}]= fun_tighten(LFRef,LFLab==lab,LFDims);
        
        % Force all views to be selected
        SRDims{lab}{4} = LFDims{4};
        SRDims{lab}{5} = LFDims{5};
        
        sz_ = cellfun(@numel,SRDims{lab});
        SRMask{lab} = true(sz_);
        SRRef{lab} = LFRef(SRDims{lab}{:});
        SRRec{lab} = nan(sz_);
    end
    
    marginLocal=15;
    addMargin = @(x,maxId) max(1,x(1)-marginLocal):min(maxId,x(end)+marginLocal);
    SRDimsExt = cellfun(@(srdim) {addMargin(srdim{1},LFDims{1}(end)), addMargin(srdim{2},LFDims{2}(end)), srdim{3:end}},SRDims,'UniformOutput',false);
end

%% Save input parameters
if ~exist(resuDir,'dir'), mkdir(resuDir); end
save(fullfile(resuDir,'initParams.mat'),...
    'uRange','vRange','crop',...
    'Order','SubOrder','QPList',...
    'FFTParams','numLayers','lambdaCalib','lambdaConstruct');

%% Save reference
refDir  = fullfile(resuDir,'Reference');
if ~exist(refDir,'dir'), mkdir(refDir); end

LF.toSubAps(LFRef,refDir,'ext','png','name',LFName);

%% Calibration of FDL using input views
[ViewsFFT,FFTParams] = FFT.computeFFT(LFRef,FFTParams);
% Prevent crash when gpu is not initiated properly (CUDA bug?)
try
    [U,V,D] = FDL.calibrate(ViewsFFT,U_ref,V_ref,D_ref,numLayers,lambdaCalib,'fixed_UV',false,'fixed_D',false);
catch
    [U,V,D] = FDL.calibrate(ViewsFFT,U_ref,V_ref,D_ref,numLayers,lambdaCalib,'fixed_UV',false,'fixed_D',false);
end

calibParamsMat = fullfile(resuDir,'calibParams.mat');
% save(calibParamsMat,'U','V','D');

if local
    Dglobal = D;
    D = cell(1,numlab);
end

%% Encode U,V,D
% WIP

%% Prediction + residual coding
for QP = QPList
    numIt = max(Order(:));
    if(isnan(QP) && numIt>2)
        warning(['Only 2 view subsets (known views and views to be predicted) can be defined when using QP=NaN (view synthesis only mode)' 10 ...
            'Grouping the predicted view subsets...']);
        Order = min(Order,2);
        numIt=max(Order(:));
    end
    
    %% Initialize reconstructed lightfield
    LFRec = zeros(size(LFRef));
    LFRes = zeros(size(LFRef));
    
    %if local
    %    [scale(:)] = deal({ones(prod(angSize),nChan)});
    %else
        scale = ones(prod(angSize),nChan);
    %end
    nbBits = nan(angSize);
    bitrate = nan(angSize);
    
    predDir = fullfile(resuDir,['Prediction_',num2str(QP)]);
    if ~exist(predDir,'dir'), mkdir(predDir); end
    predResDir = fullfile(resuDir,['Prediction_Residual_',num2str(QP)]);
    if ~exist(predResDir,'dir'), mkdir(predResDir); end
    recDir  = fullfile(resuDir,['Reconstruction_',num2str(QP)]);
    if ~exist(recDir,'dir'), mkdir(recDir); end
    recResDir = fullfile(resuDir,['Reconstruction_Residual_',num2str(QP)]);
    if ~exist(recResDir,'dir'), mkdir(recResDir); end
    resuMat = fullfile(resuDir,['Results_',num2str(QP),'.mat']);
    
    HEVCRefDir = fullfile(resuDir,['HEVC_Ref_',num2str(QP)]);
    if ~exist(HEVCRefDir,'dir'), mkdir(HEVCRefDir); end
    HEVCRecDir = fullfile(resuDir,['HEVC_Rec_',num2str(QP)]);
    if ~exist(HEVCRecDir,'dir'), mkdir(HEVCRecDir); end
    
    save(resuMat,'imgSize','nChan','angSize','numPix','numPix_All');
    
    for it = 1:numIt
        name = [LFName,'_',num2str(it),'_',num2str(QP)];
        
        refFilename = fullfile(HEVCRefDir,name);
        recFilename = fullfile(HEVCRecDir,name);
        
        HEVCParams.refFilename = refFilename;
        HEVCParams.recFilename = recFilename;
        HEVCParams.QP = num2str(QP);
        
        decodedInd = Order(:)<=it-1;
        currentInd = Order(:)==it;
        
        %% Construction of Fourier Disparity Layers using decoded subapertures
        if it>1
            if local
                % Need to prevent crash when gpu is not initiated properly (CUDA bug?)
                for lab = 1:numlab
                    SRRec{lab} = LFRec(SRDims{lab}{:});
                    ViewsFFT = FFT.computeFFT(SRRec{lab}(:,:,:,decodedInd(:)),FFTParams);
                    [~,~,D{lab}] = FDL.calibrate(ViewsFFT,U(decodedInd(:)),V(decodedInd(:)),...
                        D_ref,numLayers,lambdaCalib,'fixed_UV',true,'fixed_D',false);
                    %Layers = FDL.compute(ViewsFFT,U(decodedInd(:)),V(decodedInd(:)),D{lab},lambdaConstruct);
                    %rMod{lab} = FDL.model(Layers,D{lab},FFTParams);
                    
                    ViewsFFT = FFT.computeFFT(LFRec(SRDimsExt{lab}{1:3}, decodedInd(:)), FFTParams);
                    Layers = FDL.compute(ViewsFFT,U(decodedInd(:)),V(decodedInd(:)),D{lab},lambdaConstruct);
                    padL = FFTParams.padSizeX + SRDims{lab}{2}(1) - SRDimsExt{lab}{2}(1);
                    padR = FFTParams.padSizeX + SRDimsExt{lab}{2}(end) - SRDims{lab}{2}(end);
                    padT = FFTParams.padSizeY + SRDims{lab}{1}(1) - SRDimsExt{lab}{1}(1);
                    padB = FFTParams.padSizeY + SRDimsExt{lab}{1}(end) - SRDims{lab}{1}(end);
                    rMod{lab} = RenderModel(Layers,[size(Layers,1),size(Layers,2)],...
                                            [padL,padR,padT,padB],...
                                            D{lab},0,FFTParams.linearizeInput,FFTParams.gammaOffset);
                end
                
                for lab = 1:numlab
                   
                    SRRec{lab} = LFRec(SRDims{lab}{:});
                    SRRec{lab}(:,:,:,currentInd) = FDL.render(rMod{lab},U(currentInd),V(currentInd));
                end
                
                
            else
                ViewsFFT = FFT.computeFFT(LFRec(:,:,:,decodedInd(:)),FFTParams);
                Layers = FDL.compute(ViewsFFT,U(decodedInd(:)),V(decodedInd(:)),D,lambdaConstruct);
                rMod = FDL.model(Layers,D,FFTParams);
            end
        end
        
        calibParamsMat = fullfile(resuDir,['calibParams_',num2str(it),'.mat']);
        save(calibParamsMat,'U','V','D');
        
        %% Prediction of current frame layer using Fourier Disparity Layers
        if it==1
            LFRec(:,:,:,currentInd) = LFRef(:,:,:,currentInd);
        else
            if local
                for lab = 1:numlab
                    SRRec{lab}(:,:,:,currentInd) = FDL.render(rMod{lab},U(currentInd),V(currentInd));
                    %[SRRec{lab}(:,:,:,currentInd),scale{lab}(currentInd,:)] = FDL.scale(...
                    %    SRRec{lab}(:,:,:,currentInd),SRRef{lab}(:,:,:,currentInd));
                end
                LFRec = fun_compute_lf_sum(SRRec,SRMask,SRDims);
                
            else
                LFRec(:,:,:,currentInd) = FDL.render(rMod,U(currentInd),V(currentInd));
            end
            
            [LFRec(:,:,:,currentInd),scale(currentInd,:)] = FDL.scale(LFRec(:,:,:,currentInd),LFRef(:,:,:,currentInd));
            
            LFRes(:,:,:,currentInd) = LFRef(:,:,:,currentInd)-LFRec(:,:,:,currentInd);
        end
        
        LF.toSubAps(LFRec,predDir,'name',LFName,...
            'ext','png','mask',reshape(currentInd,angSize));
        
        LF.toSubAps(LFRes/2+0.5,predResDir,'name',LFName,...
            'ext','png','mask',reshape(currentInd,angSize));
        
        if it==1
            LFRec(:,:,:,currentInd) = zeros(size(LFRef(:,:,:,currentInd)));
            LFRes(:,:,:,currentInd) = LFRef(:,:,:,currentInd);
        end
        
        %% Encoding of residual frame layer
        if it==1
            residualInd = currentInd;
        else
            residualInd = (decodedInd|currentInd)&Order(:)>1;%current and previously decoded except from initial coded subset (why not using only currentInd here???)
        end
        
        temp = SubOrder(residualInd);
        [~,sig] = sort(temp,'ascend');
        
        residualIndSig = find(residualInd);
        residualIndSig = residualIndSig(sig);%Indices of residualInd sorted according to coding order.
        
        %curResInd = indices corresponding to the same views as in currentInd but expressed w.r.t. views selected by residualIndSig.
        [~,~,curResInd] = intersect(find(currentInd),residualIndSig,'stable');
        
        TempRes = LFRes(:,:,:,residualIndSig);
        
        if(~isnan(QP))
            if it==1
                TempRes = uint8(255*TempRes);
                [TempRes,nbBitsRes] = LF.codec(TempRes,HEVCParams,'InputBitDepth', '8');
                TempRes = double(TempRes)/255;
            else
                TempRes = uint16(511*TempRes+511);
                [TempRes,nbBitsRes] = LF.codec(TempRes,HEVCParams,'InputBitDepth','10');%?? If it>2, that will re-encode previously coded residuals (from all previous subsets except the initial one)!!!
                TempRes = (double(TempRes)-511)/511;
            end
        
            LFRec(:,:,:,currentInd) = LFRec(:,:,:,currentInd)+TempRes(:,:,:,curResInd);

            LF.toSubAps(LFRec,recDir,'name',LFName,...
                'ext','png','mask',reshape(residualInd,angSize));

            LF.toSubAps((LFRef-LFRec)/2+0.5,recResDir,'name',LFName,...
                'ext','png','mask',reshape(residualInd,angSize));

            nbBits(residualIndSig) = nbBitsRes;
            bitrate(residualIndSig) = nbBitsRes'./numPix(residualIndSig);
        else
            LFRec(:,:,:,currentInd) = LFRec(:,:,:,currentInd)+TempRes(:,:,:,curResInd);
        end
    end
    
    if(~isnan(QP))
        save(resuMat,'scale','nbBits','bitrate','-append');

        nbBits_all = sum(nbBits(:));
        bitrate_all = nbBits_all./numPix_All;
    
        save(resuMat,'nbBits_all','bitrate_all','-append');
    end
    
end

toc(tStart)
end