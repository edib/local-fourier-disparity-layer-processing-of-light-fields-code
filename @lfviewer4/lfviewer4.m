classdef lfviewer4 < handle
    %LFV_SYNC Summary of this class goes here
    %   Detailed explanation goes here
    
    properties(SetObservable,AbortSet)
        data
        
        xyuv
        
        lfv
        lfvinv
        lfv1
        lfv2
        lfv3
        lfv4
        
        h = []
        ax = []
        axinv = []
        ax1 = []
        ax2 = []
        ax3 = []
        ax4 = []
        
        s = [1,1,1,1,1]
        m = 0
    end
    
    methods
        function obj = lfviewer4(data,s)
            %LFVIEWER4 Construct an instance of this class
            %   Detailed explanation goes here
            
            if ~exist('s','var'); s = [1,1,1,1,1]; end
            
            obj.data = data;
            obj.xyuv = [1,1,1,1];
            obj.s = s;
            
            obj.initializeAxes();
            obj.setListeners();
        end
    end
    
    methods(Hidden)
        function setListeners(obj)
            addlistener(obj,'xyuv','PostSet',@(src,evnt) obj.update_cv(obj.lfv4,[1,2]));
            addlistener(obj,'xyuv','PostSet',@(src,evnt) obj.update_cv(obj.lfv3,[1,3]));
            addlistener(obj,'xyuv','PostSet',@(src,evnt) obj.update_cv(obj.lfv2,[4,2]));
            addlistener(obj,'xyuv','PostSet',@(src,evnt) obj.update_cv(obj.lfv1,[3,4]));
            addlistener(obj.lfv4,'cv','PostSet',@(src,evnt) obj.update_xyuv(obj.lfv4,[1,2]));
            addlistener(obj.lfv3,'cv','PostSet',@(src,evnt) obj.update_xyuv(obj.lfv3,[1,3]));
            addlistener(obj.lfv2,'cv','PostSet',@(src,evnt) obj.update_xyuv(obj.lfv2,[4,2]));
            addlistener(obj.lfv1,'cv','PostSet',@(src,evnt) obj.update_xyuv(obj.lfv1,[3,4]));
            addlistener(obj,'data','PostSet',@(src,evnt) obj.updateViewers());
            addlistener(obj,'s','PostSet',@(src,evnt) obj.updateAxes());
            addlistener(obj,'m','PostSet',@(src,evnt) obj.updateAxes());
            addlistener(obj.ax1,'XLim','PostSet',@(src,evnt) obj.updateAxe(obj.ax1,obj.ax3,'XLim'));
            addlistener(obj.ax3,'XLim','PostSet',@(src,evnt) obj.updateAxe(obj.ax3,obj.ax1,'XLim'));
            addlistener(obj.ax1,'YLim','PostSet',@(src,evnt) obj.updateAxe(obj.ax1,obj.ax2,'YLim'));
            addlistener(obj.ax2,'YLim','PostSet',@(src,evnt) obj.updateAxe(obj.ax2,obj.ax1,'YLim'));
            addlistener(obj.ax2,'XLim','PostSet',@(src,evnt) obj.updateAxe(obj.ax2,obj.ax4,'XLim'));
            addlistener(obj.ax4,'XLim','PostSet',@(src,evnt) obj.updateAxe(obj.ax4,obj.ax2,'XLim'));
            addlistener(obj.ax3,'XLim','PostSet',@(src,evnt) obj.updateAxe(obj.ax3,obj.ax4,'YLim'));
            addlistener(obj.ax4,'XLim','PostSet',@(src,evnt) obj.updateAxe(obj.ax4,obj.ax3,'YLim'));
        end
        
        function update_cv(obj,lfv,ind)
            lfv.cv = obj.xyuv(ind);
        end
        
        function update_xyuv(obj,lfv,ind)
            obj.xyuv(ind) = lfv.cv;
        end
        
        function updateViewers(obj)
            obj.lfv1.data = obj.data;
            obj.lfv2.data = obj.data;
            obj.lfv3.data = obj.data;
            obj.lfv4.data = obj.data;
            
            obj.updateAxes();
        end
        
        function initializeAxes(obj)
            obj.setAxes();
            obj.updateAxes();
        end
        
        function updateAxe(~,ax1,ax2,prop)
            ax2.(prop) = ax1.(prop);
        end
        
        function updateAxes(obj)
            obj.s(end+1:5) = 1;
            
            x = obj.s(1).*size(obj.data,1);
            y = obj.s(2).*size(obj.data,2);
            u = obj.s(3).*size(obj.data,4);
            v = obj.s(4).*size(obj.data,5);
            m = obj.s(5).*obj.m;
            
            xmv = x+m+v;
            x_ = x/xmv;
            v_ = v/xmv;
            vm = v+m;
            vm_ = vm/xmv;
            
            ymu = y+m+u;
            y_ = y/ymu;
            ym = y+m;
            ym_ = ym/ymu;
            u_ = u/ymu;
            
            obj.ax1.Position = [0,vm_,y_,x_];
            obj.ax2.Position = [ym_,vm_,u_,x_];
            obj.ax3.Position = [0,0,y_,v_];
            obj.ax4.Position = [ym_,0,u_,v_];
        end
        
        function setAxes(obj,style)
            if ~exist('style','var'); style = 'normal'; end
            
            if isempty(obj.h) || ~isvalid(obj.h)
                obj.h = figure();
            end
            
            obj.ax1 = subplot(2,2,1,'Parent',obj.h);
            obj.ax2 = subplot(2,2,2,'Parent',obj.h);
            obj.ax3 = subplot(2,2,3,'Parent',obj.h);
            obj.ax4 = subplot(2,2,4,'Parent',obj.h);
            
            obj.lfv1 = lfviewer(obj.data,'dims','xyvu','ax',obj.ax1);
            obj.lfv2 = lfviewer(obj.data,'dims','xuvy','ax',obj.ax2);
            obj.lfv3 = lfviewer(obj.data,'dims','vyxu','ax',obj.ax3);
            obj.lfv4 = lfviewer(obj.data,'dims','vuxy','ax',obj.ax4);
            
            axis(obj.ax1,style)
            axis(obj.ax2,style)
            axis(obj.ax3,style)
            axis(obj.ax4,style)
            
            obj.ax1.Visible = 'off';
            obj.ax2.Visible = 'off';
            obj.ax3.Visible = 'off';
            obj.ax4.Visible = 'off';
            
            obj.ax1.ActivePositionProperty = 'position';
            obj.ax2.ActivePositionProperty = 'position';
            obj.ax3.ActivePositionProperty = 'position';
            obj.ax4.ActivePositionProperty = 'position';
            
            obj.setCallbacks();
        end
        
        function setCallbacks(obj)
            obj.h.KeyPressFcn = [];
            obj.h.WindowButtonDownFcn = [];
            obj.h.WindowButtonUpFcn = [];
            obj.h.WindowButtonMotionFcn = [];
            
            obj.h.WindowButtonDownFcn = @obj.ButtonDownCallback;
            obj.h.WindowButtonUpFcn = @obj.ButtonUpCallback;
        end
        
        function ButtonDownCallback(obj,~,~)
            obj.setCurrentAxes();
            if isempty(obj.ax); return; end
            
            lfvs = [obj.lfv1,obj.lfv2,obj.lfv3,obj.lfv4];
            
            ind = arrayfun(@(lfv) isequal(obj.ax,lfv.ax),lfvs);
            obj.lfv = lfvs(ind);
            obj.lfvinv = lfvs(flip(ind));
            if isempty(obj.lfv); return; end
            
            obj.h.WindowButtonMotionFcn = @obj.ButtonMotionCallback;
        end
        
        function ButtonUpCallback(obj,~,~)
            obj.h.WindowButtonMotionFcn = '';
        end
        
        function ButtonMotionCallback(obj,~,~)
            axSize = [diff(obj.lfv.ax.YLim),diff(obj.lfv.ax.XLim)];
            axPos = obj.lfv.ax.CurrentPoint(1,[2,1]);
            mv = max(min(axPos./axSize,[1,1]),[0,0]);
            
            switch obj.h.SelectionType
                case 'normal'
                    obj.lfvinv.cv = [1,1] + round(mv.*(obj.lfvinv.maxv-[1,1]));
                case 'alt'
                    obj.lfv.cv = [1,1] + round(mv.*(obj.lfv.maxv-[1,1]));
            end
        end
        
        function setCurrentAxes(obj)
            p = get(0,'PointerLocation');
            hPos = obj.h.Position;
            
            x = (p(1)-hPos(1))/hPos(3);
            y = (p(2)-hPos(2))/hPos(4);
            
            axs = findobj(obj.h.Children,'Type','Axes');
            
            for cax = axs'
                hUnit = cax.Units;
                cax.Units = 'norm';
                r = cax.Position;
                cax.Units = hUnit;
                
                if ( (x>r(1)) && (x<r(1)+r(3)) && (y>r(2)) && (y<r(2)+r(4)) )
                    obj.ax = cax;
                    obj.axinv = axs(cax==flip(axs));
                    
                    return
                end
            end
            
            obj.ax = [];
            obj.axinv = [];
        end
    end
end