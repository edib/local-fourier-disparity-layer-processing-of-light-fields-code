function A = clamp(A,varargin)
%CLAMP Summary of this function goes here
%   Detailed explanation goes here

if nargin>1
    range = varargin{1};
else
    range = getrangefromclass(A);
end

if nargin>2
    nanflag = varargin{2};
else
    nanflag = 'omitnan';
end

A = min(max(A,range(1),nanflag),range(2),nanflag);

end