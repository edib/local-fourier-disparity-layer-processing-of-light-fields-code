function [Lab,Off,varargout] = join(Labs,Offs,varargin)
%ALIGN Summary of this function goes here
%   Detailed explanation goes here

nvararg = nargin-2;
numLab = numel(Labs);
Maxs = cell(size(Labs));

filter = logical([1,1,0,0,0]);

for lab = 1:numLab
    if ~isempty(Offs{lab}); break; end
end

Off = Offs{lab};
numDim = numel(Off);
Max = size(Labs{lab});
Max(end+1:numDim) = 1;
Max = Max+Off;

for lab = 1:numLab
    if isempty(Labs{lab}); continue; end
    
    Maxs{lab} = size(Labs{lab});
    Maxs{lab}(end+1:numDim) = 1;
    Maxs{lab} = Maxs{lab} + Offs{lab};
    
    Off = min(Offs{lab},Off);
    Max = max(Maxs{lab},Max);
end

Size = Max-Off;
Lab = zeros(Size);

[varargout,Size_it,Max_it,Maxs_it] = deal(cell(1,nvararg));

for it = 1:nvararg
    for lab = 1:numLab
        if ~isempty(varargin{it}{lab})
            break
        end
    end
    
    numDim = numel(Off);
    Max_it{it} = size(varargin{it}{lab});
    Max_it{it}(end+1:numDim) = 1;
    Max_it{it} = Max_it{it}+Off;
    
    for lab = 1:numLab
        if isempty(varargin{it}{lab}); continue; end
        
        Maxs_it{it}{lab} = size(varargin{it}{lab});
        Maxs_it{it}{lab}(end+1:numDim) = 1;
        Maxs_it{it}{lab} = Maxs_it{it}{lab} + Offs{lab};
        
        Max_it{it} = max(Maxs_it{it}{lab},Max);
    end
    
    Size_it{it} = Max_it{it}-Off;
    varargout{it} = nan(Size_it{it});
end

for lab = 1:numLab
    if isempty(Labs{lab}); continue; end
    
    padPre  = Offs{lab}-Off;
    padPost = Max-Maxs{lab};
    
    padPre (~filter) = 0;
    padPost(~filter) = 0;
    
    Lab_ = padarray(Labs{lab},padPre ,0,'pre' );
    Lab_ = padarray(Lab_     ,padPost,0,'post');
    
    Lab(Lab_==lab) = lab;
    
    for it = 1:nvararg
        %padPre  = Offs{lab}-Off;
        padPost = Max_it{it}-Maxs_it{it}{lab};
        
        %padPre (~filter) = 0;
        padPost(~filter) = 0;
        
        varargout_ = padarray(varargin{it}{lab},padPre ,nan,'pre' );
        varargout_ = padarray(varargout_        ,padPost,nan,'post');
        
        Lab_it = Lab_.*ones(Size_it{it});
        varargout{it}(Lab_it==lab) = varargout_(Lab_it==lab);
    end
end

end