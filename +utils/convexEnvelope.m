function [bitrate,peaksnr] = convexEnvelope(bitrate,peaksnr)
%CONVEX_ENVELOPE Compute and plot upper convex hull of 2D
%points
%   Detailed explanation goes here

assert(isequal(size(bitrate),size(peaksnr)));
if isscalar(bitrate)
else
    conv_ind = convhull(bitrate(:),peaksnr(:));
    bitrate = bitrate(conv_ind); peaksnr = peaksnr(conv_ind);   % Convex Hull
    
    min_bit_ind = find(bitrate==min(bitrate));    % Min Bitrate Indices
    max_bit_ind = find(bitrate==max(bitrate));    % Max Bitrate Indices
    
    [~,min_psnr_ind] = max(peaksnr(min_bit_ind)); % Max PSNR Index
    [~,max_psnr_ind] = max(peaksnr(max_bit_ind)); % Max PSNR Index
    
    min_ind = min_bit_ind(min_psnr_ind);          % Min Bitrate Max PSNR Index
    max_ind = max_bit_ind(max_psnr_ind);          % Max Bitrate Max PSNR Index
    
    shift_ind = circshift(1:numel(conv_ind),1-max_ind);  % Shift hull indices so max bitrate is the first entry
    [~,shift_max_ind] = find(shift_ind==max_ind); % Shift Max Bitrate
    [~,shift_min_ind] = find(shift_ind==min_ind); % Shift Min Bitrate
    
    inds = shift_ind(shift_max_ind:shift_min_ind);% Indices corresponding to upper convex hull
    
    bitrate = flip(bitrate(inds));
    peaksnr = flip(peaksnr(inds));
end
end