function [range,nbits,casttypename] = getRange(typename)
%GETRANGE Summary of this function goes here
%   Detailed explanation goes here

[typename,nbits,casttypename] = utils.precision(typename);

switch typename
    case {'ubit','uint'}
        range = [0 2^nbits-1];
    case { 'bit', 'int'}
        range = [-2^(nbits-1) 2^(nbits-1)-1];
    otherwise
        range = [0 1];
end

end