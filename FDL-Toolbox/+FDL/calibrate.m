function [U,V,D] = calibrate(ViewsFFT,U,V,D,numDisp,lambda,varargin)

[~,~,fullResY,fullResX] = size(ViewsFFT);
[wx,wy,~,~] = GenerateFrequencyGrid(fullResX,fullResY);

tic;fprintf('FDL Calibration...');

[~,~,U,V,D] = CalibrateFDL_UVD_gpu(ViewsFFT,wx,wy,numDisp,lambda,...
    U,V,D,varargin{:});

t=toc; fprintf(['(' num2str(t) 's)\n']);

end