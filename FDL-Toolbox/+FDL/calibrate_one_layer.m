function [U,V,D] = calibrate_one_layer(ViewsFFT,U,V,D,numDisp,lambda,varargin)

[~,~,fullResY,fullResX] = size(ViewsFFT);
[wx,wy,~,~] = GenerateFrequencyGrid(fullResX,fullResY);

%if isempty(D); D = 0; end
D = 0;

tic;fprintf('FDL Calibration (one layer)...');

[~,~,~,~,D] = CalibrateFDL_UVD_gpu(ViewsFFT,wx,wy,1,lambda,...
    U,V,D,varargin{:});

D = repelem(D,numDisp);

t=toc; fprintf(['(' num2str(t) 's)\n']);
fprintf('FDL Calibration...');

[~,~,U,V,D] = CalibrateFDL_UVD_gpu(ViewsFFT,wx,wy,numDisp,lambda,...
    U,V,D,varargin{:});

t=toc; fprintf(['(' num2str(t) 's)\n']);

end