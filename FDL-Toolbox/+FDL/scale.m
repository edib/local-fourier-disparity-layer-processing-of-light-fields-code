function [LFRec,scale] = scale(LFRec,LFRef)
%SCALE Summary of this function goes here
%   Detailed explanation goes here

[fullResY,fullResX,nChan,nViews] = size(LFRec);
scale = ones(nViews,nChan);

for it = 1:nViews
    refIm  = reshape(LFRef(:,:,:,it),[],nChan);
    predIm = reshape(LFRec(:,:,:,it),[],nChan);
    
    for chan = 1:nChan
        scale(it,chan) = ...
            (predIm(:,chan)'*refIm(:,chan))./(predIm(:,chan)'*predIm(:,chan));
        LFRec(:,:,chan,it) = ...
            min(scale(it,chan).*reshape(predIm(:,chan),[fullResY,fullResX]),1);
    end
end

end