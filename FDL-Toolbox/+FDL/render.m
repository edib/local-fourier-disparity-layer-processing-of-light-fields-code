function LFRec = render(rMod,U,V)
%RENDER Summary of this function goes here
%   Detailed explanation goes here

[fullResY,fullResX,nChan] = size(rMod.Image);
nViews = numel(U);

LFRec = nan(fullResY,fullResX,nChan,nViews);

for it = 1:nViews
    rMod.setPosition(U(it),V(it));
    rMod.renderImage();
    
    LFRec(:,:,:,it) = double(rMod.Image)/255;
end

end