function rMod = model(Layers,D,varargin)
%RENDERMODEL Summary of this function goes here
%   Detailed explanation goes here

p = inputParser; p.StructExpand = true; p.KeepUnmatched = true;
p.addParameter('linearizeInput', false, @islogical);
p.addParameter('gammaOffset'   , 0    , @isnumeric);
p.addParameter('padSizeX'      , []   , @isnumeric);
p.addParameter('padSizeY'      , []   , @isnumeric);

p.parse(varargin{:});

[fullResY,fullResX,~] = size(Layers);
imgSizeX = fullResX - 2*p.Results.padSizeX;
imgSizeY = fullResY - 2*p.Results.padSizeY;

DispMap = zeros([imgSizeY,imgSizeX],'single');

tic;fprintf('FDL Model Construction...');

rMod = RenderModel(Layers,[fullResY,fullResX],...
    [p.Results.padSizeX,p.Results.padSizeX,p.Results.padSizeY,p.Results.padSizeY],...
    D,DispMap,p.Results.linearizeInput,p.Results.gammaOffset);

t=toc;fprintf(['(' num2str(t) 's)\n']);

end