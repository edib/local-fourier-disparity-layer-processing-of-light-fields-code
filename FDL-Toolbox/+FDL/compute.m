function Layers = compute(ViewsFFT,U,V,D,lambda)

[~,~,fullResY,fullResX] = size(ViewsFFT);
[wx,wy,~,~] = GenerateFrequencyGrid(fullResX,fullResY);

tic; fprintf('FDL Construction...');

Layers = ComputeFDL_gpu(ViewsFFT,wx,wy,U,V,D,lambda);

t=toc; fprintf(['(' num2str(t) 's)\n']);

end