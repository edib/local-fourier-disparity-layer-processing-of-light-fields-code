function LFRef = computeViews(ViewsFFT,varargin)
%CONVERTToFOURIER Summary of this function goes here
%   Detailed explanation goes here

% Create input parser scheme
p = inputParser; p.StructExpand = true; p.KeepUnmatched = true;
p.addParameter('linearizeInput' , false       , @islogical);
p.addParameter('gammaOffset'    , 0           , @isnumeric);
p.addParameter('padSize'        , [15,15]     , @isnumeric);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%  Input Light Field parameters %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

p.parse(varargin{:});
linearizeInput = p.Results.linearizeInput; %Use inverse gamma correction to process images in linear space.
gammaOffset    = p.Results.gammaOffset;    %Offset applied before inverse gamma correction.
padSize        = p.Results.padSize;        %Window profile type, 'hann' or 'linear'

useGPU = parallel.gpu.GPUDevice.isAvailable; %Use gpu for initilisation (Windowing + Fourier Transform of input views).

padSizeX=padSize(2); %Number of padded pixels on left and right borders.
padSizeY=padSize(1); %Number of padded pixels on top and bottom borders.

p.addParameter('padSizeX', padSizeX, @isnumeric);
p.addParameter('padSizeY', padSizeY, @isnumeric);

p.parse(varargin{:});

padSizeX = p.Results.padSizeX;
padSizeY = p.Results.padSizeY;

%% Prepare variables
numInitViews = size(ViewsFFT,1);
nChan = size(ViewsFFT,2);
fullResY = size(ViewsFFT,3);
fullResX = size(ViewsFFT,4);

InitViewIds = 1:numInitViews; %Selected views for the input of the FDL construction.

if(useGPU)
    ViewsFFT = gpuArray(ViewsFFT);
end

ViewsFFT = permute(ViewsFFT,[3 4 2 1]);

%% Pre-process input data (Padding/Windowing + Fourier Tramsform )
if(useGPU)
    LFRef = zeros([fullResY,fullResX,nChan,numInitViews],'single','gpuArray');
else
    LFRef = zeros([fullResY,fullResX,nChan,numInitViews],'single');
end

for l=1:numInitViews
    idView = InitViewIds(l);
    
    if(useGPU)
        LFRef(:,:,:,idView) = ifft2(ifftshift(ifftshift(ViewsFFT(:,:,:,l),2),1));
    else
        for ch=1:nChan, LFRef(:,:,ch,idView) = ifft2(ifftshift(ViewsFFT(:,:,ch,l))); end
    end
end

LFRef = LFRef(1+padSizeY:end-padSizeY,1+padSizeX:end-padSizeX,:,:);

LFRef = real(LFRef);

if(linearizeInput)
    LFRef = BT709_gammaEncode(LFRef)-gammaOffset;
end

if(useGPU)
    LFRef = gather(LFRef);
end

end