classdef lfv_sync < handle
    %LFV_SYNC Summary of this class goes here
    %   Detailed explanation goes here
    
    properties(SetObservable,AbortSet)
        xyuv
        
        lfv
        lfvinv
        lfv1
        lfv2
        lfv3
        lfv4
        
        h
        ax
        axinv
        ax1
        ax2
        ax3
        ax4
    end
    
    methods
        function obj = lfv_sync(data)
            %LFV_SYNC Construct an instance of this class
            %   Detailed explanation goes here
            
            obj.xyuv = [1,1,1,1];
            
            obj.initialize(data);
            
            addlistener(obj,'xyuv','PostSet',@(src,evnt) obj.update_cv(obj.lfv4,[1,2]));
            addlistener(obj,'xyuv','PostSet',@(src,evnt) obj.update_cv(obj.lfv3,[1,3]));
            addlistener(obj,'xyuv','PostSet',@(src,evnt) obj.update_cv(obj.lfv2,[4,2]));
            addlistener(obj,'xyuv','PostSet',@(src,evnt) obj.update_cv(obj.lfv1,[3,4]));
            addlistener(obj.lfv4,'cv','PostSet',@(src,evnt) obj.update_xyuv(obj.lfv4,[1,2]));
            addlistener(obj.lfv3,'cv','PostSet',@(src,evnt) obj.update_xyuv(obj.lfv3,[1,3]));
            addlistener(obj.lfv2,'cv','PostSet',@(src,evnt) obj.update_xyuv(obj.lfv2,[4,2]));
            addlistener(obj.lfv1,'cv','PostSet',@(src,evnt) obj.update_xyuv(obj.lfv1,[3,4]));
        end
    end
    
    methods(Hidden)
        function update_cv(obj,lfv,ind)
            %UPDATE Summary of this method goes here
            %   Detailed explanation goes here
            
            lfv.cv = obj.xyuv(ind);
        end
        
        function update_xyuv(obj,lfv,ind)
            %UPDATE Summary of this method goes here
            %   Detailed explanation goes here
            
            obj.xyuv(ind) = lfv.cv;
        end
        
        function initialize(obj,data)
            %INITIALIZE Summary of this method goes here
            %   Detailed explanation goes here
            
            obj.h = figure();
            
            obj.ax1 = subplot(2,2,1,'Parent',obj.h);
            obj.ax2 = subplot(2,2,2,'Parent',obj.h);
            obj.ax3 = subplot(2,2,3,'Parent',obj.h);
            obj.ax4 = subplot(2,2,4,'Parent',obj.h);
            
            obj.lfv1 = lfviewer(data,'dims','xyvu','ax',obj.ax1);
            obj.lfv2 = lfviewer(data,'dims','xuvy','ax',obj.ax2);
            obj.lfv3 = lfviewer(data,'dims','vyxu','ax',obj.ax3);
            obj.lfv4 = lfviewer(data,'dims','vuyx','ax',obj.ax4);
            
            %%
            style = 'normal';
            axis(obj.ax1,style)
            axis(obj.ax2,style)
            axis(obj.ax3,style)
            axis(obj.ax4,style)
            
            %%
            x = size(data,1);
            y = size(data,2);
            u = size(data,4);
            v = size(data,5);
            m = 10;
            
            suv = 1;20;
            sx = v;
            sy = u;
            su = x;suv*u;
            sv = y;suv*v;
            sm = 1;
            
            x = sx*x;
            y = sy*y;
            u = su*u;
            v = sv*v;
            m = sm*m;
            
            xmv = x+m+v;
            x_ = x/xmv;
            v_ = v/xmv;
            vm = v+m;
            vm_ = vm/xmv;
            
            ymu = y+m+u;
            y_ = y/ymu;
            ym = y+m;
            ym_ = ym/ymu;
            u_ = u/ymu;
            
            obj.ax1.Visible = 'off';
            obj.ax2.Visible = 'off';
            obj.ax3.Visible = 'off';
            obj.ax4.Visible = 'off';
            
            obj.ax1.ActivePositionProperty = 'position';
            obj.ax2.ActivePositionProperty = 'position';
            obj.ax3.ActivePositionProperty = 'position';
            obj.ax4.ActivePositionProperty = 'position';
            
            obj.ax1.Position = [0,vm_,y_,x_];
            obj.ax2.Position = [ym_,vm_,u_,x_];
            obj.ax3.Position = [0,0,y_,v_];
            obj.ax4.Position = [ym_,0,u_,v_];
            
            obj.h.KeyPressFcn = [];
            obj.h.WindowButtonDownFcn = [];
            obj.h.WindowButtonUpFcn = [];
            obj.h.WindowButtonMotionFcn = [];
            
            obj.h.WindowButtonDownFcn = @obj.ButtonDownCallback;
            obj.h.WindowButtonUpFcn = @obj.ButtonUpCallback;
        end
        
        function ButtonDownCallback(obj,~,~)
            obj.getCurrentAxes();
            if isempty(obj.ax); return; end
            
            lfvs = [obj.lfv1,obj.lfv2,obj.lfv3,obj.lfv4];
            
            ind = arrayfun(@(lfv) isequal(obj.ax,lfv.ax),lfvs);
            obj.lfv = lfvs(ind);
            obj.lfvinv = lfvs(flip(ind));
            if isempty(obj.lfv); return; end
            
            obj.h.WindowButtonMotionFcn = @obj.ButtonMotionCallback;
        end
        
        function ButtonUpCallback(obj,~,~)
            obj.h.WindowButtonMotionFcn = '';
        end
        
        function ButtonMotionCallback(obj,~,~)
            axSize = [diff(obj.lfv.ax.YLim),diff(obj.lfv.ax.XLim)];
            axPos = obj.lfv.ax.CurrentPoint(1,[2,1]);
            mv = max(min(axPos./axSize,[1,1]),[0,0])
            
            switch obj.h.SelectionType
                case 'normal'
                    obj.lfvinv.cv = [1,1] + round(mv.*(obj.lfvinv.maxv-[1,1]));
                case 'alt'
                    obj.lfv.cv = [1,1] + round(mv.*(obj.lfv.maxv-[1,1]));
            end
        end
        
        function getCurrentAxes(obj)
            p = get(0,'PointerLocation');
            hPos = obj.h.Position;
            
            x = (p(1)-hPos(1))/hPos(3);
            y = (p(2)-hPos(2))/hPos(4);
            
            axs = findobj(obj.h.Children,'Type','Axes');
            
            for cax = axs'
                hUnit = cax.Units;
                cax.Units = 'norm';
                r = cax.Position;
                cax.Units = hUnit;
                
                if ( (x>r(1)) && (x<r(1)+r(3)) && (y>r(2)) && (y<r(2)+r(4)) )
                    obj.ax = cax;
                    break
                end
            end
            
            if ~( (x>r(1)) && (x<r(1)+r(3)) && (y>r(2)) && (y<r(2)+r(4)) )
                obj.ax = [];
                obj.axinv = [];
            else
                axind = obj.ax==flip(axs);
                obj.axinv = axs(axind);
            end
            
        end
        
    end
end