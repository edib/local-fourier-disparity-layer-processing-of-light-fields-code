classdef lfviewer < handle
    %VIEWER Summary of this class goes here
    %   Detailed explanation goes here
    
    properties(SetObservable,AbortSet)
        data
        dims = 'xy'
        name = 'lf'
        range = 'class'
        cv = [1,1]
        maxv = [1,1]
        perm = [1,2,3,4,5]
        slice
        clim
        title
    end
    
    properties
        im = []
        ax = []
    end
    
    properties(Hidden)
        validRanges = {'class','slice','data'};
        climClass
        climData
        climSlice
        gv
    end
    
    methods
        function obj = lfviewer(data,varargin)
            p = inputParser; p.StructExpand = true; p.KeepUnmatched = true;
            
            p.addRequired('data');
            p.addParameter('dims' , obj.dims  ,@ischar);
            p.addParameter('name' , obj.name  ,@ischar);
            p.addParameter('range', obj.range ,@(range) any(strcmpi(range,obj.validRanges)));
            p.addParameter('cv'   , obj.cv    ,@isnumeric);
            p.addParameter('maxv' , obj.maxv  ,@isnumeric);
            p.addParameter('ax'   , obj.ax    ,@(x) isempty(x) || isgraphics(x,'Axes'));
            
            p.parse(data,varargin{:});
            
            res = p.Results;
            for fname = fieldnames(res)'
                fn = fname{:};
                obj.(fn) = res.(fn);
            end
            
            obj.cv = obj.cv;
            obj.updateData();
            obj.view();
            
            addlistener(obj, 'data' , 'PostSet', @(src,evnt) obj.updateData);
            addlistener(obj, 'dims' , 'PostSet', @(src,evnt) obj.updatePerm);
            addlistener(obj, 'perm' , 'PostSet', @(src,evnt) obj.updateTitle);
            addlistener(obj, 'perm' , 'PostSet', @(src,evnt) obj.updateSlice);
            addlistener(obj, 'maxv' , 'PostSet', @(src,evnt) obj.clip);
            addlistener(obj, 'cv'   , 'PostSet', @(src,evnt) obj.updateTitle);
            addlistener(obj, 'cv'   , 'PostSet', @(src,evnt) obj.updateSlice);
            addlistener(obj, 'cv'   , 'PostSet', @(src,evnt) obj.clip);
            addlistener(obj, 'slice', 'PostSet', @(src,evnt) obj.updateImage);
            addlistener(obj, 'name' , 'PostSet', @(src,evnt) obj.updateTitle);
            addlistener(obj, 'clim' , 'PostSet', @(src,evnt) obj.updateImage);
            addlistener(obj, 'range', 'PostSet', @(src,evnt) obj.updateCLim);
        end
        
        function view(obj,ax,style)
            if ~exist('ax','var'); ax = obj.ax; end
            if ~exist('style','var'); style = 'image'; end
            
            obj.ax = ax;
            
            if isempty(obj.ax) || ~isvalid(obj.ax)
                h = figure();
                obj.ax = axes(h);
                obj.ax.Colormap = gray;
            end
            
            if isempty(obj.im) || ~isvalid(obj.im)
                obj.im = imagesc(obj.ax,obj.slice);
            else
                obj.im.Parent = obj.ax;
            end
            
            axis(obj.ax,style);
            
            obj.ax.Parent.KeyPressFcn         = @obj.KeyPressCallback;
            obj.ax.Parent.WindowButtonDownFcn = @obj.ButtonDownCallback;
            obj.ax.Parent.WindowButtonUpFcn   = @obj.ButtonUpCallback;
            
            obj.updateCLim();
            obj.updateTitle();
            obj.updateImage();
        end
    end
    
    methods(Hidden)
        function clip(obj)
            obj.cv = min(obj.maxv,max([1,1],obj.cv));
            obj.cv = obj.cv;
        end
        
        function updateData(obj)
            obj.gv = arrayfun(@(x) 1:x,size(obj.data),'UniformOutput',false);
            
            if isa(obj.data,'gpuArray')
                cu = classUnderlying(obj.data);
                obj.climClass = getrangefromclass(ones(1,cu));
            else
                obj.climClass = getrangefromclass(obj.data);
            end
            
            data_ = obj.data(~isinf(obj.data(:)));
            obj.climData = double([min(data_,[],'omitnan'),max(data_,[],'omitnan')]);
            obj.maxv = arrayfun(@(x) size(obj.data,x),obj.perm(4:5));
            
            if isempty(obj.climData) || any(isnan(obj.climData))
                obj.climData = obj.climClass;
            end
            
            if diff(obj.climData)==0
                obj.climData(1) = -inf;
            end
            
            obj.updateCLim();
            obj.updateTitle();
            obj.updatePerm();
            obj.updateSlice();
        end
        
        function updatePerm(obj)
            dims_ = [obj.dims,setdiff('uxvy',obj.dims,'stable')];
            [~,perm_] = sort(dims_,'ascend');
            perm_ = perm_([3,4,1,2]);
            perm_(perm_) = 1:4;
            perm_(perm_>2) = perm_(perm_>2)+1;
            perm_ = [perm_(1:2),3,perm_(3:4)];
            
            obj.maxv = arrayfun(@(x) size(obj.data,x),perm_(4:5));
            obj.perm = perm_;
        end
        
        function updateSlice(obj)
            gv_ = obj.gv;
            ccv = num2cell(obj.cv);
            [gv_{obj.perm(4:5)}] = deal(ccv{:});
            empty_ = cellfun(@isempty,gv_);
            gv_(empty_) = deal({1});
            
            slice_ = permute(obj.data(gv_{:}),obj.perm);
            obj.climSlice = double(...
                [min(slice_(~isinf(slice_(:))),[],'omitnan')...
                ,max(slice_(~isinf(slice_(:))),[],'omitnan')]);
            obj.slice = slice_;
        end
        
        function updateCLim(obj)
            switch lower(obj.range)
                case 'class'
                    clim_ = obj.climClass;
                case 'slice'
                    clim_ = obj.climSlice;
                case 'data'
                    clim_ = obj.climData;
            end
            
            if isempty(clim_) || any(isnan(clim_))
                clim_ = obj.climData;
            end
            
            if diff(clim_)==0
                clim_(1) = -inf;
            end
            
            if ~isempty(obj.ax) && isvalid(obj.ax)
                obj.ax.CLim = clim_;
            end
            
            obj.clim = clim_;
        end
        
        function updateTitle(obj)
            if isempty(obj.name)
                if ~isempty(obj.ax) && isvalid(obj.ax) &&...
                        ~isempty(obj.ax.Title.String)
                    obj.ax.Title.String = '';
                end
                return
            end
            
            coordStr = {':',':',':',':',':'};
            coordStr(obj.perm(4:5)) = arrayfun(@num2str,obj.cv,'UniformOutput',false);
            coordStr = ['(' strjoin(coordStr,','),')'];
            
            obj.title = [obj.name,' ',coordStr];
            
            if ~isempty(obj.ax) && isvalid(obj.ax)
                obj.ax.Title.String = obj.title;
            end
        end
        
        function updateImage(obj)
            if isempty(obj.im) || ~isvalid(obj.im)
                return
            end
            
            if isa(obj.slice,'gpuArray')
                obj.im.CData = gather(obj.slice);
            else
                obj.im.CData = obj.slice;
            end
            
            obj.im.AlphaData = double(any(~isnan(obj.im.CData),3));
        end
        
        function KeyPressCallback(obj,~,evnt)
            switch evnt.Key
                case    'uparrow'
                    obj.cv(1) = obj.cv(1)-1;
                case  'downarrow'
                    obj.cv(1) = obj.cv(1)+1;
                case  'leftarrow'
                    obj.cv(2) = obj.cv(2)-1;
                case 'rightarrow'
                    obj.cv(2) = obj.cv(2)+1;
                case 'space'
                    rangeInd = find(strcmp(obj.range,obj.validRanges));
                    rangeInd = mod(rangeInd,3)+1;
                    obj.range = obj.validRanges{rangeInd};
            end
        end
        
        function ButtonDownCallback(obj,~,~)
            refView = obj.cv;
            refPos = obj.ax.CurrentPoint(1,[2 1]);
            
            obj.ax.Parent.WindowButtonMotionFcn = ...
                @(src,evnt) (...
                obj.ButtonMotionCallback(src,evnt,refView,refPos));
        end
        
        function ButtonUpCallback(obj,~,~)
            obj.ax.Parent.WindowButtonMotionFcn = '';
        end
        
        function ButtonMotionCallback(obj,~,~,refView,refPos)
            axSize = [diff(obj.ax.YLim),diff(obj.ax.XLim)];
            axPos = obj.ax.CurrentPoint(1,[2,1]);
            
            switch obj.ax.Parent.SelectionType
                case 'normal'
                    mv = 2.*(axPos - refPos)./axSize.*obj.maxv;
                    obj.cv = refView + round(mv);
                case 'alt'
            end
        end
    end
end