addpath(genpath('FDL-Toolbox'));
addpath('utils');
addpath('fun');

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%  Parameters for FDL calibration + construction %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% lambdaCalib = 1;
% lambdaConstruct = .1;
numLayers = 30;

FFTParams.gpuInit        = true;
FFTParams.padType        = 'symmetric';
FFTParams.padSize        = [15,15];
FFTParams.Windowing      = true;
FFTParams.windowProfile  = 'hann';
FFTParams.linearizeInput = false;
FFTParams.gammaOffset    = 0;

%% Input and output directories
RefDir = 'D:\Thesis\LightFields\LightFields_RGB';
RecDir = 'D:\Thesis\Experiments\FDL_local_synthesis';

s = dir(RefDir);
s = s([s.isdir]);
% LFNames = {s(3:end).name};
% LFNames = {'Buddha','StillLife','Bikes','Fountain_&_Vincent_2'};
% LFNames = {'Buddha','Fountain_&_Vincent_2'};
LFNames = {'Buddha','StillLife'};

range_1_9 = {'StillLife','Buddha','Greek','Sideboard','Butterfly'};
range_5_13 = {'tarot','lego_knights'};

% TestOrders = {'Hierarchical_2x2','Hierarchical_3x3','Hierarchical_5x5'};
TestOrders = {'Circular_2'};

%% Run algorithm
%% Select subaperture coding order
for OrderName = TestOrders
    OrderName = OrderName{:}
    
    for LFName = LFNames
        LFName = LFName{:}
        
        if any(strcmp(LFName,{'Danger_de_Mort','Duck','Sideboard'}))
            continue
        end
        
%         if strcmp(OrderName,'Circular_2')% && any(strcmp(LFName,LFNames(1:12)))
%             continue
%         end
        
        %% Load light field
        %list of v indices of the images to load from the files.
        
        switch LFName
            case range_1_9
                uRange = 1:9;
                vRange = 1:9;
                crop = [0 0 0 0];
            case range_5_13
                uRange = 5:13;
                vRange = 5:13;
                crop = [0 0 0 0];
            otherwise
                uRange = 4:12;
                vRange = 4:12;
                crop = [0 0 0 0];
        end
        
        QPList = nan;%set to nan to skip HEVC encoding (only to tet prediction from original view subset).
        
        LFRef = loadLF(fullfile(RefDir,LFName), LFName, 'png', uRange, vRange, crop);
        LFRef = double(LFRef)/double(intmax(class(LFRef)));
        
        try
            LFLab = loadLF(fullfile(RefDir,LFName,'Segmentation'), LFName, 'png', uRange, vRange, crop);
        catch
            LFLab = [];
            continue
        end
        
%         warning('FORCING GLOBAL!!!!!!');
%         LFLab = [];
        
        %%
        resuDir = fullfile(RecDir,OrderName,LFName);
        
        %%
        nU = numel(uRange);
        nV = numel(vRange);
        
        m = matfile(['./LayerConfigurations/',num2str(nU),'x',num2str(nV),'_',OrderName],'Writable',false);
        Order    = m.Order;
        if(~ismember(who(m),'SubOrder'))
            warning('SubOrder is not defined in configuration file. Using default z-scan SubOrder.')
            SubOrder = reshape(1:nU*nV,size(Order));
        else
            SubOrder = m.SubOrder;
        end
        
        %%
        folder_name = ['FDL_Params_',num2str(numLayers)];
        file_name = [LFName,'_',OrderName,'.mat'];
        m = matfile(fullfile(folder_name,file_name));
        
        lambdaCalib = m.lambdaCalibFull;
        lambdaConstruct = m.lambdaConstructFull;
        
        %% Run algorithm
        fun_encode_fdl(resuDir,LFName,...
            LFRef,uRange,vRange,crop,Order,SubOrder,QPList,...
            FFTParams,numLayers,lambdaCalib,lambdaConstruct,LFLab);
        fun_compute_quality_metrics(resuDir,LFName,QPList);
    end
end