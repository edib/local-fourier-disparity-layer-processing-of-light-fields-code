function [color,mask,dims,gv] = fun_tighten(color,mask,dims)
%FUN_TIGHTEN Summary of this function goes here
%   Detailed explanation goes here

if ~exist('mask','var')
    if islogical(color); mask = color; else; mask = ~isnan(color); end
end

if ~exist('dims','var')
    dims = arrayfun(@(x) 1:x,size(color),'UniformOutput',false);
end

mask = logical(mask);
numdims = ndims(mask);
gv = cell(1,numdims);

for dim = 1:numdims
    M = any(mask,setdiff(1:numdims,dim));
    gv{dim} = find(M,1,'first'):find(M,1,'last');
end

repdims = size(color)./size(mask);

mask = mask(gv{:});

for dim = find(repdims~=1)
    gv{dim} = 1:size(color,dim);
end

color = color(gv{:});

dims = cellfun(@(dim,gv) dim(gv),dims,gv,'UniformOutput',false);

end