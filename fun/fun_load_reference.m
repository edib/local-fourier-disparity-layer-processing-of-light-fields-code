function varargout = fun_load_reference(refdir,varargin)
%FUN_LOAD_REFERENCE Summary of this function goes here
%   Detailed explanation goes here

subdirs = fullfile(refdir,varargin);
varargout = cellfun(...
    @(subdir) fun_read_subapertures(subdir,'ext','png'),subdirs,'UniformOutput',false);

end