function [srcol,srmask,srdims,lfcol,lflab,lfdims] = fun_compute_sr(...
    lfcol,lflab,lfdims,varargin)
%FUN_COMPUTE_SR Summary of this function goes here
%   Detailed explanation goes here

%% Parse arguments
p = inputParser; p.KeepUnmatched = true; p.StructExpand = true;

p.addParameter('sequential', isempty(gcp('nocreate')) , @islogical);
p.addParameter('padsize'   , 0                        , @isnumeric);

p.parse(varargin{:});

sequential = p.Results.sequential;
padsize    = p.Results.padsize;

crop = zeros(1,ndims(lfcol));
sub  = ones (1,ndims(lfcol));
pad  = zeros(1,ndims(lfcol));
pad(1:2) = padsize;

p.addParameter('croppre' , crop, @isnumeric);
p.addParameter('croppost', crop, @isnumeric);
p.addParameter('subsamp' , sub , @isnumeric);
p.addParameter('padpre'  , pad , @isnumeric);
p.addParameter('padpost' , pad , @isnumeric);

p.parse(varargin{:});

croppre  = p.Results.croppre;
croppost = p.Results.croppost;
subsamp  = p.Results.subsamp;
padpre   = p.Results.padpre;
padpost  = p.Results.padpost;

%% Start chronometer
tStart = tic;
fprintf('Super-ray creation started\n');

%% Initialize variables
numLab = max(lflab(:));
[srcol,srmask,srdims] = deal(cell(1,numLab));

%% Post-process color and label
 lflab         = fun_crop_subsamp(lflab,croppre,croppost,subsamp,lfdims);
[lfcol,lfdims] = fun_crop_subsamp(lfcol,croppre,croppost,subsamp,lfdims);

 lflab         = fun_pad(lflab,padpre,padpost,lfdims,0  );
[lfcol,lfdims] = fun_pad(lfcol,padpre,padpost,lfdims,nan);

%% Compute super-rays using light field color + label
se = strel('square',2*padsize+1);

if sequential
    for lab = 1:numLab
        mask = imdilate(lflab==lab,se);
        [srcol{lab},~,srdims{lab}] = fun_tighten(lfcol     ,mask,lfdims);
        [srmask{lab}]              = fun_tighten(lflab==lab,mask,lfdims);
    end
else
    parfor lab = 1:numLab
        mask = imdilate(lflab==lab,se);
        [srcol{lab},~,srdims{lab}] = fun_tighten(lfcol     ,mask,lfdims);
        [srmask{lab}]              = fun_tighten(lflab==lab,mask,lfdims);
    end
end

%% Stop chronometer
tEnd = toc(tStart);
fprintf(['Super-ray creation finished after ',num2str(tEnd),' seconds\n']);

end