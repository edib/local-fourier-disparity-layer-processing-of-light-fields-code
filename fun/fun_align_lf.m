function [lfrefcol_,lfreflab_,lfrefdims_,lfreccol_,lfreclab_,lfrecdims_] = fun_align_lf(lfrefcol,lfreflab,lfrefdims,lfreccol,lfreclab,lfrecdims)
%FUN_ALIGN_LF Summary of this function goes here
%   Detailed explanation goes here

%%
lfrefmin = cellfun(@min,lfrefdims); lfrefmax = cellfun(@max,lfrefdims);
lfrecmin = cellfun(@min,lfrecdims); lfrecmax = cellfun(@max,lfrecdims);

lfmin = arrayfun(@min,lfrefmin,lfrecmin);
lfmax = arrayfun(@max,lfrefmax,lfrecmax);

lfdims = arrayfun(@(min_,max_) min_:max_,lfmin,lfmax,'UniformOutput',false);
lfsize = cellfun(@numel,lfdims);

%%
[lfrefcol_,lfreccol_] = deal(nan(lfsize));
[lfreflab_,lfreclab_] = deal(nan(lfsize));
[lfrefdims_,lfrecdims_] = deal(lfdims);

%%
[~,inds,subinds] = cellfun(@(lfdim,dim) intersect(lfdim,dim,'stable'),...
    lfdims,lfrefdims,'UniformOutput',false);

lfrefcol_(inds{:}) = lfrefcol(subinds{:});
lfreflab_(inds{:}) = lfreflab(subinds{:});

%%
[~,inds,subinds] = cellfun(@(lfdim,dim) intersect(lfdim,dim,'stable'),...
    lfdims,lfrecdims,'UniformOutput',false);

lfreccol_(inds{:}) = lfreccol(subinds{:});
lfreclab_(inds{:}) = lfreclab(subinds{:});

end