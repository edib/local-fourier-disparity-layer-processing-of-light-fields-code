function [color,mask] = fun_warp(color,mask,dims,DMax,inverse,complete,sequential)
%FUN_WARP Summary of this function goes here
%   Detailed explanation goes here

if ~exist('inverse'   ,'var'); inverse    = false; end
if ~exist('complete'  ,'var'); complete   = false; end
if ~exist('sequential','var'); sequential = false; end

fun = @(color,mask,dims,DMax) fun_warp_aux(color,mask,dims,DMax,inverse,complete);

if iscell(color)
    numlab = numel(color);
    labels = find(~cellfun(@isempty,color));
    
    if sequential
        for lab = 1:numlab
            if ~any(lab==labels); continue; end
            [color{lab},mask{lab}] = fun(color{lab},mask{lab},dims{lab},DMax{lab});
        end
    else
        parfor lab = 1:numlab
            if ~any(lab==labels); continue; end
            [color{lab},mask{lab}] = fun(color{lab},mask{lab},dims{lab},DMax{lab});
        end
    end
else
    [color,mask] = fun(color,mask,dims,DMax);
end

    function [color,mask] = fun_warp_aux(color,mask,dims,DMax,inverse,complete)
        lfsize = size(color);
        lfsize(end+1:5) = 1;
        imgres = lfsize(4:end);
        
        if inverse
            [~,TMax] = fun_affine_to_homography(DMax,imgres);
        else
               TMax  = fun_affine_to_homography(DMax,imgres);
        end
        
        [color,mask] = fun_warp_homography(color,mask,TMax,dims);
        
        if complete
            mask = any(mask,[4,5]);
        end
        
        mask = mask | false(lfsize);
        color(~mask) = nan;
        mask = any(mask,3);
    end
end