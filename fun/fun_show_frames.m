function fun_show_frames(frames,name,dt,rg)
%FUN_SHOW_FRAMES Summary of this function goes here
%   Detailed explanation goes here

if ~exist('name','var'); name = 'frame'; end
if ~exist('dt'  ,'var'); dt   = 0; end
if ~exist('rg'  ,'var'); rg   = getrangefromclass(frames{1}); end

figure;
ax = gca;
for it = 1:numel(frames)
    imshow(frames{it},rg,'Parent',ax);
    title(ax,[name,' ',num2str(it)])
    if dt
        pause(dt);
    end
end
end