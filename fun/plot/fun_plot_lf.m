function  fun_plot_lf(lf)
%FUN_PLOT_LF Summary of this function goes here
%   Detailed explanation goes here

lfSize = size(lf);
lfSize(end+1:5) = 1;
imgSize = lfSize(1:3);
imgRes = lfSize(4:5);

figure

%% Plot B matrix
lftab = lf2frames(lf);
lftab = cell2mat(lftab);
imagesc(lftab);
ax = gca;
ax.XTick = cumsum(repelem(imgSize(2),imgRes(2)))+0.5;
ax.YTick = cumsum(repelem(imgSize(1),imgRes(1)))+0.5;
ax.XTickLabel = arrayfun(@num2str,1:imgRes(2),'UniformOutput',false);
ax.YTickLabel = arrayfun(@num2str,1:imgRes(1),'UniformOutput',false);
ax.Colormap = gray;
grid on
ax.GridAlpha = 1;
axis image

title('lightfield');
ylabel('u')
xlabel('v')

end