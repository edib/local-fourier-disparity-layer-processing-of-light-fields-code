function  fun_plot_compare_C(C1,C2,imgRes)
%FUN_PLOT_COMPARE_C Summary of this function goes here
%   Detailed explanation goes here

if ~exist('imgRes','var'); imgRes = [size(C1,2),1]; end
k = size(C1,1);

figure

%% Plot C (transforms) matrix
Ctab = mat2frames(C1',imgRes,[k,1]);
Ctab = cell2mat(Ctab);
subplot(1,2,1)
imagesc(Ctab);
ax = gca;
ax.XTick = cumsum(repelem(imgRes(2),1))+0.5;
ax.YTick = cumsum(repelem(imgRes(1),k))+0.5;
ax.XTickLabel = [];
ax.YTickLabel = arrayfun(@num2str,1:k,'UniformOutput',false);
grid on
ax.GridAlpha = 1;
axis image
title('C_1');

%% Plot C (transforms) matrix
Ctab = mat2frames(C2',imgRes,[k,1]);
Ctab = cell2mat(Ctab);
subplot(1,2,2)
imagesc(Ctab);
ax = gca;
ax.XTick = cumsum(repelem(imgRes(2),1))+0.5;
ax.YTick = cumsum(repelem(imgRes(1),k))+0.5;
ax.XTickLabel = [];
ax.YTickLabel = arrayfun(@num2str,1:k,'UniformOutput',false);
grid on
ax.GridAlpha = 1;
axis image
title('C_2');

end