function  fun_plot_B_C_T(B,C,T,imgRes,blims,clims,tlims)
%FUN_PLOT_B_C_T Summary of this function goes here
%   Detailed explanation goes here

if ~exist('imgRes','var'); imgRes = [size(C,2),1]; end
if ~exist('blims','var'); blims = []; end
if ~exist('clims','var'); clims = []; end
if ~exist('tlims','var'); tlims = []; end

imgSize = size(B);
k = size(C,1);
%k = size(B,4);

figure

%%
B = -B;
B(:,:,:,1) = B(:,:,:,1) - mean(B(:,:,:,1),'all');

%% Plot B matrix
Btab = lf2frames(B);
Btab = cell2mat(Btab);
subplot(1,3,1)
imagesc(Btab);
ax = gca;
ax.XTick = cumsum(repelem(imgSize(2),1))+0.5;
ax.YTick = cumsum(repelem(imgSize(1),k))+0.5;
ax.XTickLabel = [];
ax.YTickLabel = arrayfun(@num2str,1:k,'UniformOutput',false);
if ~isempty(blims); ax.CLim = blims; end
ax.Colormap = gray;
grid on
ax.GridAlpha = 1;
axis image
title('B');
ylabel('eigen image index')

%% Plot C (transforms) matrix
Ctab = mat2frames(C',imgRes,[k,1]);
Ctab = cell2mat(Ctab);
%figure
subplot(1,3,2)
imagesc(Ctab);
ax = gca;
ax.XTick = cumsum(repelem(imgRes(2),1))+0.5;
ax.YTick = cumsum(repelem(imgRes(1),k))+0.5;
ax.XTickLabel = [];
ax.YTickLabel = arrayfun(@num2str,1:k,'UniformOutput',false);
if ~isempty(clims); ax.CLim = clims; end
grid on
ax.GridAlpha = 1;
axis image
title('C');

%% Plot T (transforms) matrix
Ttab = mat2frames(T',imgRes,[3,3]);
Ttab = cell2mat(Ttab)';
subplot(1,3,3)
imagesc(Ttab);
ax = gca;
ax.XTick = cumsum(repelem(imgRes(2),3))+0.5;
ax.YTick = cumsum(repelem(imgRes(1),3))+0.5;
ax.XTickLabel = arrayfun(@num2str,1:3,'UniformOutput',false);
ax.YTickLabel = arrayfun(@num2str,1:3,'UniformOutput',false);
if ~isempty(tlims); ax.CLim = tlims; end
grid on
ax.GridAlpha = 1;
axis image
title('T');
end