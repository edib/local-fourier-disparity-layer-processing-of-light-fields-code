function  fun_plot_B_C(B,C,imgRes)
%FUN_PLOT_B_C Summary of this function goes here
%   Detailed explanation goes here

if ~exist('imgRes','var'); imgRes = [size(C,2),1]; end

imgSize = size(B);
k = size(C,1);

figure

%%
B = -B;
B(:,:,:,1) = B(:,:,:,1) - mean(B(:,:,:,1),'all');

%% Plot B matrix
Btab = lf2frames(B);
Btab = cell2mat(Btab);
subplot(1,4,2)
imagesc(Btab);
ax = gca;
ax.XTick = cumsum(repelem(imgSize(2),1))+0.5;
ax.YTick = cumsum(repelem(imgSize(1),k))+0.5;
ax.XTickLabel = [];
ax.YTickLabel = arrayfun(@num2str,1:k,'UniformOutput',false);

ax.Colormap = gray;
grid on
ax.GridAlpha = 1;
axis image
title('B');
ylabel('eigen image index')

%% Plot C (transforms) matrix
Ctab = mat2frames(C',imgRes,[k,1]);
Ctab = cell2mat(Ctab);
%figure
subplot(1,4,3)
imagesc(Ctab);
ax = gca;
ax.XTick = cumsum(repelem(imgRes(2),1))+0.5;
ax.YTick = cumsum(repelem(imgRes(1),k))+0.5;
ax.XTickLabel = [];
ax.YTickLabel = arrayfun(@num2str,1:k,'UniformOutput',false);
grid on
ax.GridAlpha = 1;
axis image
title('C');

end