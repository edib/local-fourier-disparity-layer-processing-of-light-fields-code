function fun_plot_T(T,imgRes,uv,minor)
%FUN_PLOT_T Summary of this function goes here
%   Detailed explanation goes here

if ~exist('imgRes','var'); imgRes = [size(T,2),1]; end
if ~exist('uv','var'); uv = true; end
if ~exist('minor','var'); minor = false; end

figure

if uv
%% Plot T (transforms) matrix
Ttab = mat2frames(T',imgRes,[3,3]);
Ttab = cell2mat(Ttab)';
imagesc(Ttab);
ax = gca;
ax.XTick = cumsum(repelem(imgRes(2),3))+0.5;
ax.YTick = cumsum(repelem(imgRes(1),3))+0.5;
ax.XTickLabel = arrayfun(@num2str,1:3,'UniformOutput',false);
ax.YTickLabel = arrayfun(@num2str,1:3,'UniformOutput',false);
grid on
ax.GridAlpha = 1;
axis image
title('T');

if minor
    grid minor
    ax.XAxis.MinorTick = 'on';
    ax.YAxis.MinorTick = 'on';
    ax.XAxis.MinorTickValues = cumsum(repelem(1,size(Ttab,2)))+0.5;
    ax.YAxis.MinorTickValues = cumsum(repelem(1,size(Ttab,1)))+0.5;
    ax.MinorGridAlpha = 1;
end

else
%% Plot T (transforms) matrix
Ttab = mat2frames(T,[3,3],imgRes);
Ttab = cell2mat(Ttab)';
imagesc(Ttab);
ax = gca;
ax.XTick = cumsum(repelem(3,imgRes(2)))+0.5;
ax.YTick = cumsum(repelem(3,imgRes(1)))+0.5;
ax.XTickLabel = arrayfun(@num2str,1:imgRes(2),'UniformOutput',false);
ax.YTickLabel = arrayfun(@num2str,1:imgRes(1),'UniformOutput',false);
grid on
ax.GridAlpha = 1;
axis image
title('T');

if minor
    grid minor
    ax.XAxis.MinorTick = 'on';
    ax.YAxis.MinorTick = 'on';
    ax.XAxis.MinorTickValues = cumsum(repelem(1,size(Ttab,2)))+0.5;
    ax.YAxis.MinorTickValues = cumsum(repelem(1,size(Ttab,1)))+0.5;
    ax.MinorGridAlpha = 1;
end
end

end