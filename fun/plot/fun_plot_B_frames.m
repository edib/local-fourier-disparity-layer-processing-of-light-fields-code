function fun_plot_B_frames(B,center)
%FUN_PLOT_B_FRAMES Summary of this function goes here
%   Detailed explanation goes here

if ~exist('center','var'); center = true; end

B = -B;
if center
    offset = B(:,:,:,1);
    offset = (max(offset(:))+min(offset(:)))/2;
    B(:,:,:,1) = B(:,:,:,1) - offset;
end

B = (B-min(B(:)))/(max(B(:))-min(B(:)));
frames_B_ = lf2frames(B);
fun_show_frames(frames_B_,'eigen image',1,[]);

end