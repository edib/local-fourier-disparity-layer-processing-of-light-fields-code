function  fun_plot_B(B,kList,normalize,clims)
%FUN_PLOT_B Summary of this function goes here
%   Detailed explanation goes here

if ~exist('kList','var'); kList = 1:size(B,4); end
if ~exist('normalize','var'); normalize = true; end

imgSize = size(B);

figure

%%
B = -B;

if normalize; B(:,:,:,1) = B(:,:,:,1) - mean(B(:,:,:,1),'all'); end
if ~exist('clims','var'); clims = [min(B(:)),max(B(:))]; end

B = B(:,:,:,kList);

%% Plot B matrix
Btab = lf2frames(B);
Btab = cell2mat(Btab);
imagesc(Btab,clims);
ax = gca;
ax.XTick = [];
ax.YTick = cumsum(repelem(imgSize(1),numel(kList)))+0.5;
ax.XTickLabel = [];
ax.YTickLabel = arrayfun(@num2str,kList,'UniformOutput',false);

ax.Colormap = gray;
grid on
ax.GridAlpha = 1;
axis image
title('B');

end