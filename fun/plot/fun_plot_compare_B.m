function  fun_plot_compare_B(B1,B2)
%FUN_PLOT_compare_B Summary of this function goes here
%   Detailed explanation goes here

imgSize = size(B1);
k = size(B1,4);

figure

%%
B1 = -B1;
B1(:,:,:,1) = B1(:,:,:,1) - mean(B1(:,:,:,1),'all');
% B = (B-min(B(:)))/(max(B(:))-min(B(:)));

%%
B2 = -B2;
B2(:,:,:,1) = B2(:,:,:,1) - mean(B2(:,:,:,1),'all');
% B = (B-min(B(:)))/(max(B(:))-min(B(:)));

%%
% B3 = B1-B2;

%% Plot C matrix
Btab = lf2frames(B1);
Btab = cell2mat(Btab);
subplot(1,3,1)
imagesc(Btab);
ax = gca;
ax.XTick = cumsum(repelem(imgSize(2),1))+0.5;
ax.YTick = cumsum(repelem(imgSize(1),k))+0.5;
ax.XTickLabel = [];
ax.YTickLabel = arrayfun(@num2str,1:k,'UniformOutput',false);

ax.Colormap = gray;
grid on
ax.GridAlpha = 1;
axis image
title('B_1');

%% Plot C matrix
Btab = lf2frames(B2-B1);
Btab = cell2mat(Btab);
subplot(1,3,2)
imagesc(Btab);
ax = gca;
ax.XTick = cumsum(repelem(imgSize(2),1))+0.5;
ax.YTick = cumsum(repelem(imgSize(1),k))+0.5;
ax.XTickLabel = [];
ax.YTickLabel = arrayfun(@num2str,1:k,'UniformOutput',false);

ax.Colormap = gray;
grid on
ax.GridAlpha = 1;
axis image
title('B_2-B_1');

%% Plot C matrix
Btab = lf2frames(B2);
Btab = cell2mat(Btab);
subplot(1,3,3)
imagesc(Btab);
ax = gca;
ax.XTick = cumsum(repelem(imgSize(2),1))+0.5;
ax.YTick = cumsum(repelem(imgSize(1),k))+0.5;
ax.XTickLabel = [];
ax.YTickLabel = arrayfun(@num2str,1:k,'UniformOutput',false);

ax.Colormap = gray;
grid on
ax.GridAlpha = 1;
axis image
title('B_2');

end