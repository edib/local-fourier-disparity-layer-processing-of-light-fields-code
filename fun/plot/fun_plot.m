function fun_plot(lfref,lfrec,B,C,T)
%FUN_PLOT Summary of this function goes here
%   Detailed explanation goes here

lfSize = size(lfref);
lfSize(end+1:4) = 1;
imgRes  = lfSize(4:end);

%% Plot C (weights) matrix
fun_plot_C(C,imgRes);

%% Plot T (transforms) matrix
fun_plot_T(T,imgRes);

%% Plot eigen images
fun_plot_B_frames(B);

%% Plot warped low rank approxiation
fun_plot_BC_frames(B,C,imgRes);

%% Plot reconstructed light field
fun_plot_lf_frames(lfrec);

%% Plot residual light field
fun_plot_lf_res_frames(lfrec,lfref);

end