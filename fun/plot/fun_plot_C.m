function  fun_plot_C(C,imgRes,uv,minor)
%FUN_PLOT_C Summary of this function goes here
%   Detailed explanation goes here

if ~exist('imgRes','var'); imgRes = [size(C,2),1]; end
if ~exist('uv','var'); uv = true; end
if ~exist('minor','var'); minor = false; end

k = size(C,1);

figure

if uv
%% Plot C (transforms) matrix
Ctab = mat2frames(C',imgRes,[k,1]);
Ctab = cell2mat(Ctab);
imagesc(Ctab);
ax = gca;
ax.XTick = cumsum(repelem(imgRes(2),1))+0.5;
ax.YTick = cumsum(repelem(imgRes(1),k))+0.5;
ax.XTickLabel = [];
ax.YTickLabel = arrayfun(@num2str,1:k,'UniformOutput',false);
grid on
ax.GridAlpha = 1;
axis image
title('C');

if minor
grid minor
ax.XAxis.MinorTick = 'on';
ax.YAxis.MinorTick = 'on';
ax.XAxis.MinorTickValues = cumsum(repelem(1,size(Ctab,2)))+0.5;
ax.YAxis.MinorTickValues = cumsum(repelem(1,size(Ctab,1)))+0.5;
ax.MinorGridAlpha = 1;
end

else
%% Plot C matrix
Ctab = mat2frames(C,[k,1],imgRes);
Ctab = cell2mat(Ctab);
imagesc(Ctab);
ax = gca;
ax.XTick = cumsum(repelem(1,imgRes(2)))+0.5;
ax.YTick = cumsum(repelem(k,imgRes(1)))+0.5;
ax.XTickLabel = arrayfun(@num2str,1:imgRes(2),'UniformOutput',false);
ax.YTickLabel = arrayfun(@num2str,1:imgRes(1),'UniformOutput',false);
grid on
ax.GridAlpha = 1;
axis image
title('C');

if minor
    grid minor
    ax.XAxis.MinorTick = 'on';
    ax.YAxis.MinorTick = 'on';
    ax.XAxis.MinorTickValues = cumsum(repelem(1,size(Ctab,2)))+0.5;
    ax.YAxis.MinorTickValues = cumsum(repelem(1,size(Ctab,1)))+0.5;
    ax.MinorGridAlpha = 1;
end
end

end