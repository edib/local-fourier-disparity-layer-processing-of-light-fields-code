function fun_plot_BC_frames(B,C,imgRes)
%FUN_PLOT_BC_frames Summary of this function goes here
%   Detailed explanation goes here

if ~exist('imgRes','var'); imgRes = [size(C,2),1]; end

BSize = size(B);
BSize(end+1:4) = 1;
imgSize = BSize(1:3);
k = BSize(4);

numPix  = prod(imgSize);
bmSize = [numPix,k];
MSize = [imgSize,imgRes];

M = reshape(reshape(B,bmSize)*C,MSize);
frames = lf2frames(M);
fun_show_frames(frames,'warped low rank approximation',1e-1);

end