function fun_plot_lf_res_frames(lfrec,lfref)
%FUN_PLOT_LF_RES_FRAMES Summary of this function goes here
%   Detailed explanation goes here

frames_rec = lf2frames(lfrec);
frames_ref = lf2frames(lfref);
frames_res = cellfun(@(rec,ref) 0.5+0.5*(rec-ref),frames_rec,frames_ref,'UniformOutput',false);
fun_show_frames(frames_res,'residual',1e-1,[0,1]);

end