function  fun_plot_B_C_T_compare(B1,B2,C1,C2,T1,T2,imgRes,blims,clims,tlims)
%FUN_PLOT_B_C_T Summary of this function goes here
%   Detailed explanation goes here

if ~exist('imgRes','var'); imgRes = [size(C1,2),1]; end
if ~exist('blims','var'); blims = []; end
if ~exist('clims','var'); clims = []; end
if ~exist('tlims','var'); tlims = []; end

imgSize = size(B1);
k = size(C1,1);
%k = size(B,4);

figure

%%
B1 = -B1;
B1(:,:,:,1) = B1(:,:,:,1) - mean(B1(:,:,:,1),'all');

%% Plot B matrix
Btab = lf2frames(B1);
Btab = cell2mat(Btab);
subplot(1,6,1)
imagesc(Btab);
ax1 = gca;
ax1.XTick = cumsum(repelem(imgSize(2),1))+0.5;
ax1.YTick = cumsum(repelem(imgSize(1),k))+0.5;
ax1.XTickLabel = [];
ax1.YTickLabel = arrayfun(@num2str,1:k,'UniformOutput',false);
if ~isempty(blims); ax1.CLim = blims; end
ax1.Colormap = gray;
grid on
ax1.GridAlpha = 1;
axis image
title('B_h');
ylabel('eigen image index')

%%
B2 = -B2;
B2(:,:,:,1) = B2(:,:,:,1) - mean(B2(:,:,:,1),'all');

%% Plot B matrix
Btab = lf2frames(B2);
Btab = cell2mat(Btab);
subplot(1,6,2)
imagesc(Btab);
ax2 = gca;
ax2.XTick = cumsum(repelem(imgSize(2),1))+0.5;
ax2.YTick = cumsum(repelem(imgSize(1),k))+0.5;
ax2.XTickLabel = [];
ax2.YTickLabel = arrayfun(@num2str,1:k,'UniformOutput',false);
if ~isempty(blims); ax2.CLim = blims; end
ax2.Colormap = gray;
grid on
ax2.GridAlpha = 1;
axis image
title('B_a');
ylabel('eigen image index')

%% Plot C (transforms) matrix
Ctab = mat2frames(C1',imgRes,[k,1]);
Ctab = cell2mat(Ctab);
%figure
subplot(1,6,3)
imagesc(Ctab);
ax3 = gca;
ax3.XTick = cumsum(repelem(imgRes(2),1))+0.5;
ax3.YTick = cumsum(repelem(imgRes(1),k))+0.5;
ax3.XTickLabel = [];
ax3.YTickLabel = arrayfun(@num2str,1:k,'UniformOutput',false);
if ~isempty(clims); ax3.CLim = clims; end
grid on
ax3.GridAlpha = 1;
axis image
title('C_h');

%% Plot C (transforms) matrix
Ctab = mat2frames(C2',imgRes,[k,1]);
Ctab = cell2mat(Ctab);
%figure
subplot(1,6,4)
imagesc(Ctab);
ax4 = gca;
ax4.XTick = cumsum(repelem(imgRes(2),1))+0.5;
ax4.YTick = cumsum(repelem(imgRes(1),k))+0.5;
ax4.XTickLabel = [];
ax4.YTickLabel = arrayfun(@num2str,1:k,'UniformOutput',false);
if ~isempty(clims); ax4.CLim = clims; end
grid on
ax4.GridAlpha = 1;
axis image
title('C_a');

%% Plot T (transforms) matrix
Ttab = mat2frames(T1',imgRes,[3,3]);
Ttab = cell2mat(Ttab)';
subplot(1,6,5)
imagesc(Ttab);
ax5 = gca;
ax5.XTick = cumsum(repelem(imgRes(2),3))+0.5;
ax5.YTick = cumsum(repelem(imgRes(1),3))+0.5;
ax5.XTickLabel = arrayfun(@num2str,1:3,'UniformOutput',false);
ax5.YTickLabel = arrayfun(@num2str,1:3,'UniformOutput',false);
if ~isempty(tlims); ax5.CLim = tlims; end
grid on
ax5.GridAlpha = 1;
axis image
title('T_h');

%% Plot T (transforms) matrix
Ttab = mat2frames(T2',imgRes,[3,3]);
Ttab = cell2mat(Ttab)';
subplot(1,6,6)
imagesc(Ttab);
ax6 = gca;
ax6.XTick = cumsum(repelem(imgRes(2),3))+0.5;
ax6.YTick = cumsum(repelem(imgRes(1),3))+0.5;
ax6.XTickLabel = arrayfun(@num2str,1:3,'UniformOutput',false);
ax6.YTickLabel = arrayfun(@num2str,1:3,'UniformOutput',false);
if ~isempty(tlims); ax6.CLim = tlims; end
grid on
ax6.GridAlpha = 1;
axis image
title('T_a');

linkprop([ax1,ax2],'CLim');
linkprop([ax3,ax4],'CLim');
linkprop([ax5,ax6],'CLim');
end