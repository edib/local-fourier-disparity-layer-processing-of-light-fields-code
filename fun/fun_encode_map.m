function nbBits = fun_encode_map(map,mapdir,mapname)
%FUN_ENCODE_MAP Summary of this function goes here
%   Detailed explanation goes here

if ~exist('map_dir','dir'); mkdir(mapdir); end

name_png  = [mapname,'_in.png'];
name_flif = [mapname,'.flif'  ];

name_png  = fullfile(mapdir,name_png );
name_flif = fullfile(mapdir,name_flif);

if max(map(:))<255
    map = uint8(map);
else
    map = uint16(map);
end

imwrite(map,name_png);

nbBits = FLIF.codec('encode',true,'decode',false,...
    'input',name_png,'flif',name_flif,...
    'encode_options','-ovvv -Q100 -E100 -G222 --no-subtract-green -N');

end