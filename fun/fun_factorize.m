function [B,C,M,U,S,V] = fun_factorize(M,mask,k,varargin)
%FUN_FACTORIZE Summary of this function goes here
%   Detailed explanation goes here

seh = strel('arbitrary',[1,0,1]);
sev = strel('arbitrary',[1,0,1]');

p = inputParser; p.StructExpand = true; p.KeepUnmatched = true;
p.addParameter('colDims', []  , @isnumeric);
p.addParameter('rowDims', []  , @isnumeric);
p.addParameter('resize' , true, @islogical);

p.parse(varargin{:});

colDims = p.Results.colDims;
rowDims = p.Results.rowDims;
resize  = p.Results.resize;

numDims = ndims(M);
if (isempty(colDims)&&isempty(rowDims)); colDims = 1:min(3,numDims); end
if isempty(colDims); colDims = setdiff(1:numDims,rowDims); end
if isempty(rowDims); rowDims = setdiff(1:numDims,colDims); end

colSize = arrayfun(@(dim) size(M,dim),colDims);
rowSize = arrayfun(@(dim) size(M,dim),rowDims);

mask = any(logical(mask),rowDims) | false(size(M));

while (any(isnan(M(mask(:)))))
    M = fun_fillmissing(M,seh,1);
    M = fun_fillmissing(M,sev,1);
end

M = permute(M,[colDims,rowDims]);
M = reshape(M,[prod(colSize),prod(rowSize)]);
mask = permute(mask,[colDims,rowDims]);
mask = reshape(mask,[prod(colSize),prod(rowSize)]);
mask = mask(:,1);

[U,S,V] = svd(M(mask,:),'econ');
B = nan(size(M,1),k);
B(mask,:) = -U(:,1:k)*S(1:k,1:k);
C = -V(:,1:k)';

if resize
    M = reshape(M,[colSize,rowSize]);
    M = ipermute(M,[colDims,rowDims]);
    B = reshape(B,[colSize,k]);
    B = ipermute(B,[colDims,rowDims]);
end
end