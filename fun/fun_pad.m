function [lf,dims] = fun_pad(lf,pre,post,dims,val)
%FUN_PAD Summary of this function goes here
%   Detailed explanation goes here

if ~exist('dims','var'); dims = arrayfun(@(x) 1:x,size(lf),'UniformOutput',false); end
if ~exist('val' ,'var'); val = nan; end

lf = padarray(lf,pre ,val,'pre' );
lf = padarray(lf,post,val,'post');

dims = cellfun(@(dim,pre ) padarray(dim,[0,pre ],nan,'pre' ),dims,num2cell(pre ),'UniformOutput',false);
dims = cellfun(@(dim,post) padarray(dim,[0,post],nan,'post'),dims,num2cell(post),'UniformOutput',false);

dims = cellfun(@(dim) fillmissing(dim,'linear'),dims,'UniformOutput',false);

end