function frames = mat2frames(M,imgSize,imgRes)
%MAT2FRAMES Summary of this function goes here
%   Detailed explanation goes here

frames = mat2cell(M,size(M,1),ones([1,size(M,2)]));

frames = cellfun(@(x) reshape(x,imgSize),frames,'UniformOutput',false);
frames = reshape(frames,imgRes);

end