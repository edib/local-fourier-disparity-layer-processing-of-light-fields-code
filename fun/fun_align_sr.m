function [srrefcol,srrefmask,srrefdims,srreccol,srrecmask,srrecdims] = ...
    fun_align_sr(srrefcol,srrefmask,srrefdims,srreccol,srrecmask,srrecdims)
%FUN_ALIGN_SR Summary of this function goes here
%   Detailed explanation goes here

%%
numlab = numel(srrefcol);

for lab = 1:numlab
    [
        srrefcol{lab},srrefmask{lab},srrefdims{lab},...
        srreccol{lab},srrecmask{lab},srrecdims{lab}...
        ] = fun_align_lf(...
        srrefcol{lab},srrefmask{lab},srrefdims{lab},...
        srreccol{lab},srrecmask{lab},srrecdims{lab});
    
    srrefmask{lab}(isnan(srrefmask{lab})) = 0;
    srrecmask{lab}(isnan(srrecmask{lab})) = 0;
end

end