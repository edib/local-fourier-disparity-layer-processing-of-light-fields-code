function D = fun_update_affine(I,M,D,DI,step,updateType,dims_,model,coupled)
%FUN_UPDATE_AFFINE Summary of this function goes here
%   Detailed explanation goes here

seh = strel('arbitrary',[1,0,1]);
sev = strel('arbitrary',[1,0,1]');

[~,imgSize,~,~,numView,defdims] = get_dims(I);
numPixC = prod(imgSize);

if ~exist('step','var'); step = 1; end
if ~exist('updateType','var'); updateType = 1; end
if ~exist('dims_','var'); dims_ = defdims; end
if ~exist('model','var'); model = 'affine'; end
if ~exist('coupled','var'); coupled = false; end

persistent X Y U V dims

update = isempty(dims) || ~isequal(dims_,dims);

if update
    dims = dims_;
    
    [X,Y,~,U,V] = ndgrid(dims{:});
    
    X = reshape(X,[],1);
    Y = reshape(Y,[],1);
    U = reshape(U,[],1);
    V = reshape(V,[],1);
end

mSize = [numPixC,numView];

% gradient computation
for it = 1:3
    I = fun_fillmissing(I,seh,1);
    I = fun_fillmissing(I,sev,1);
end
[Iy,Ix] = gradient(I);

Ix = reshape(Ix,[],1);
Iy = reshape(Iy,[],1);
DI = reshape(DI,[],1);
M  = reshape(M ,mSize);

M = logical(M);

if (updateType==0); M = true(size(M)); end
%if(updateType==1); M = M; end
if (updateType==2); M = repmat(any(M,2),1,size(M,2)); end
if (updateType==3); M = repmat(all(M,2),1,size(M,2)); end

M = reshape(M ,[],1);

J = [Ix.*U.*X,Ix.*U.*Y,Ix.*U,Iy.*V.*X,Iy.*V.*Y,Iy.*V];

if strcmp(model,'constant' ); mask = [0,0,1;0,0,1]'; end
if strcmp(model,'affine_x' ); mask = [1,0,1;1,0,1]'; end
if strcmp(model,'affine_y' ); mask = [0,1,1;0,1,1]'; end
if strcmp(model,'affine_xy'); mask = [1,0,1;0,1,1]'; end
if strcmp(model,'affine_yx'); mask = [0,1,1;1,0,1]'; end
if strcmp(model,'affine'   ); mask = [1,1,1;1,1,1]'; end

DI = DI(M,:);
J  = J (M,:);

maskcoupled = false(3,2);

if coupled
    maskcoupled = all(mask,2);
    
    J = reshape(J,[],3,2);
    J(:,maskcoupled) = sum(J(:,maskcoupled,:),3);
    J = reshape(J,[],6);
end

masknoncoupled = mask & ~maskcoupled;
nnzmnc = nnz(masknoncoupled);

J = [J(:,masknoncoupled),J(:,maskcoupled)];

dD_ = pinv(J)*DI;

dD = zeros(3,2);
dD(masknoncoupled) = dD_(1:nnzmnc);
dD(maskcoupled,1)  = dD_(1+nnzmnc:end);
dD(maskcoupled,2)  = dD_(1+nnzmnc:end);

D = D + step*dD(:)';

    function [lfSize,imgSize,imgRes,numPix,numView,dims] = get_dims(lfref)
        lfSize = size(lfref);
        lfSize(end+1:3) = 1;
        imgSize = lfSize(1:3);
        imgRes = lfSize(4:end);
        numPix  = prod(imgSize);
        numView = numel(lfref)/prod(imgSize);
        
        dims = arrayfun(@(x) (1:x)-floor(x/2)-1,lfSize,'UniformOutput',false);
    end
end