function T = fun_update_homography(I,M,T,DI,step,updateType,dims_)
%FUN_UPDATE_HOMOGRAPHY Summary of this function goes here
%   Detailed explanation goes here

[~,imgSize,~,~,numView,defdims] = get_dims(I);
numPixC = prod(imgSize);

if ~exist('step','var'); step = 1; end
if ~exist('updateType','var'); updateType = 1; end
if ~exist('dims_','var'); dims_ = defdims; end

persistent XYO X Y dims

update = isempty(dims) || ~isequal(dims_,dims);

if update
    dims = dims_;
    
    [X,Y] = ndgrid(dims{1:3});
    O = ones(imgSize);
    
    X = X(:);
    Y = Y(:);
    O = O(:);
    
    XYO = [X,Y,O];
end

mSize = [numPixC,numView];

T_ = T(1:8,:);

% gradient computation
[Iy,Ix] = gradient(I);

Ix = reshape(Ix,mSize);
Iy = reshape(Iy,mSize);
DI = reshape(DI,mSize);
M  = reshape(M ,mSize);

M = logical(M);

if (updateType==0); M = true(size(M)); end
%if(updateType==1); M = M; end
if (updateType==2); M = repmat(any(M,2),1,size(M,2)); end
if (updateType==3); M = repmat(all(M,2),1,size(M,2)); end

for v = 1:numView
    Ixv = Ix(:,v);
    Iyv = Iy(:,v);
    DIv = DI(:,v);
    Mv  = M (:,v);
    
    UV1 = XYO*reshape(T(:,v),[3,3]);
    U = UV1(:,1);
    V = UV1(:,2);
    W = UV1(:,3);
    
    Jv = [...
        Ixv.*X, Ixv.*Y, Ixv,...
        Iyv.*X, Iyv.*Y, Iyv,...
        -X.*(Ixv.*U+Iyv.*V)./W,...
        -Y.*(Ixv.*U+Iyv.*V)./W]...
        ./W;
    
    Jv  = Jv (Mv,:);
    DIv = DIv(Mv,:);
    
    dTv = pinv(Jv)*DIv;
    T_(:,v) = T_(:,v) + step*dTv(:);
end

T(1:8,:) = T_;

    function [lfSize,imgSize,imgRes,numPix,numView,dims] = get_dims(lfref)
        lfSize = size(lfref);
        lfSize(end+1:3) = 1;
        imgSize = lfSize(1:3);
        imgRes = lfSize(4:end);
        numPix  = prod(imgSize);
        numView = numel(lfref)/prod(imgSize);
        
        dims = arrayfun(@(x) (1:x)-floor(x/2)-1,lfSize,'UniformOutput',false);
    end
end