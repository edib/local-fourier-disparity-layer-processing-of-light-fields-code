function [lfrec,maskrec,B,C,varargout] = fun_homography(lfref,maskref,varargin)
%FUN_HOMOGRAPHY Summary of this function goes here
%   Detailed explanation goes here

[lfSize,imgSize,~,numPix,numView,dims] = get_dims(lfref);

metrics = {'psnr','ssim'};

p = inputParser; p.KeepUnmatched = true; p.StructExpand = true;

p.addParameter('k'         , 1     , @isnumeric);
p.addParameter('numiter'   , 30    , @isnumeric);
p.addParameter('metric'    , 'psnr', @(m) any(strcmpi(m,metrics)));
p.addParameter('maxout'    , true  , @islogical);
p.addParameter('verbose'   , true  , @islogical);
p.addParameter('display'   , false , @islogical);
p.addParameter('step'      , 1     , @isnumeric);
p.addParameter('update'    , 1     , @isnumeric);
p.addParameter('completion', 1     , @isnumeric);
p.addParameter('dims'      , dims  , @iscell);

p.parse(varargin{:});

k          = p.Results.k;
numIter    = p.Results.numiter;
metric     = p.Results.metric;
maxOut     = p.Results.maxout;
verbose    = p.Results.verbose;
show       = p.Results.display;
step       = p.Results.step;
update     = p.Results.update;
completion = p.Results.completion;
dims       = p.Results.dims;

%%
switch lower(metric)
    case 'psnr'
        metric = @psnr;
        mstr = 'PSNR';
        unitstr = 'dB';
    case 'ssim'
        metric = @ssim;
        mstr = 'SSIM';
        unitstr = '';
end

metricTitle = [mstr,'(',unitstr,')'];
metricOutMax = -inf;
metricInMax = -inf;

%%
bSize = [imgSize,k];
mSize = [numPix ,numView];

T = repmat(reshape(diag([1,1,1]),[],1),1,numView);
[Ti,TMax,TiMax] = deal(T);

metricIn  = nan(1,numIter);
metricOut = nan(1,numIter);

iterMax = 1;

for iter = 1:numIter
    % forward warping
    [M,maskwarp] = fun_warp_homography(lfref,maskref,T,dims);
    
    % low-rank approximation
    mmaskwarp = reshape(logical(maskwarp),mSize);
    mmaskwarp = any(mmaskwarp,2);
    M  = reshape(M,mSize);
    [U,S,V] = svd(M(mmaskwarp,:),'econ');
    B = nan(numPix,k);
    B(mmaskwarp,:) = U(:,1:k)*S(1:k,1:k);
    C = V(:,1:k)';
    
    % mask completion
    maskwarp = reshape(logical(maskwarp),mSize);
    
    if (completion==0); maskwarp = true(size(maskwarp)); end
    %if(completion==1); maskwarp = maskwarp; end
    if (completion==2); maskwarp = repmat(any(maskwarp,2),1,size(maskwarp,2)); end
    if (completion==3); maskwarp = repmat(all(maskwarp,2),1,size(maskwarp,2)); end
    
    maskwarp = reshape(double(maskwarp),lfSize);
    
    M  = reshape(M  ,lfSize);
    BC = reshape(B*C,lfSize);
    metricIn(iter) = mean_metric(BC,M,maskwarp);
    
    % inverse warping
    [lfrec,maskrec] = fun_warp_homography(BC,maskwarp,Ti,dims);
    metricOut(iter) = mean_metric(lfrec,lfref,maskrec);
    
    update_max_transform;
    
    if verbose; display_progress; end
    if show; display_graph; end
    
    % transform update
    T = fun_update_homography(M,maskwarp,T,BC-M,step,update,dims);
    Ti = inv_T(T);
end

%%
% forward warping
[M,maskrec] = fun_warp_homography(lfref,maskref,TMax,dims);

% low-rank approximation
mmaskrec = reshape(logical(maskrec),mSize);
mmaskrec = any(mmaskrec,2);
M  = reshape(M,mSize);
[U,S,V] = svd(M(mmaskrec,:),'econ');
B = nan(numPix,k);
B(mmaskrec,:) = U(:,1:k)*S(1:k,1:k);
C = V(:,1:k)';

% mask completion
maskrec = reshape(logical(maskrec),mSize);

if (completion==0); maskrec = true(size(maskrec)); end
%if(completion==1); maskrec = maskrec; end
if (completion==2); maskrec = repmat(any(maskrec,2),1,size(maskrec,2)); end
if (completion==3); maskrec = repmat(all(maskrec,2),1,size(maskrec,2)); end

maskrec = reshape(double(maskrec),lfSize);

% inverse warping
BC = reshape(B*C,lfSize);
B  = reshape(B,bSize);
[lfrec,maskrec] = fun_warp_homography(BC,maskrec,TiMax,dims);

lfrec(isnan(lfref)) = nan;

%%
varargout = {TMax,metricOutMax,metricInMax,iterMax,metricOut,metricIn};

%% Auxiliary functions
fprintf('\n');

    function res = mean_metric(rec,ref,mask)
        rec  = lf2frames(rec);
        ref  = lf2frames(ref);
        mask = lf2frames(logical(mask));
        
        res = cellfun(@(rec,ref,mask) metric(rec(mask),ref(mask)),rec,ref,mask);
        res = mean(res(:));
    end

    function update_max_transform()
        if (maxOut && metricOut(iter)>metricOutMax)...
                || (~maxOut && metricIn(iter)>metricInMax)
            iterMax = iter;
            metricOutMax = metricOut(iter);
            metricInMax = metricIn(iter);
            TMax = T;
            TiMax = inv_T(TMax);
        end
    end

    function display_progress()
        fprintf(['iter: %4d, ',mstr,'out: %3.2f ',unitstr,', ',mstr,'in: %3.2f ',unitstr,'\n'...
            'max : %4d, ',mstr,'out: %3.2f ',unitstr,', ',mstr,'in: %3.2f ',unitstr,'\n'],...
            iter,metricOut(iter),metricIn(iter),...
            iterMax,metricOutMax,metricInMax);
    end

    function display_graph()
        persistent ax
        
        if  isempty(ax)||~isvalid(ax); figure; ax = gca; end
        
        hold(ax,'off')
        ax.ColorOrderIndex = 1;
        plot(ax,metricIn,'DisplayName',[mstr,'in']);
        hold(ax,'on')
        plot(ax,metricOut,'DisplayName',[mstr,'out']);
        ax.ColorOrderIndex = 1;
        scatter(ax,iterMax,metricInMax,'+','DisplayName',[mstr,'in max']);
        scatter(ax,iterMax,metricOutMax,'+','DisplayName',[mstr,'out max']);
        
        grid(ax,'on')
        grid(ax,'minor')
        xlabel(ax,'iterations');
        ylabel(ax,metricTitle);
        title(ax,metricTitle);
        legend(ax,'show','Location','nw');
        
        drawnow
    end

    function T = inv_T(T)
        T = reshape(T,3,3,[]);
        
        for it = 1:size(T,3)
            T(:,:,it) = inv(T(:,:,it));
        end
        
        T = reshape(T,9,[]);
    end

    function [lfSize,imgSize,imgRes,numPix,numView,dims] = get_dims(lfref)
        lfSize = size(lfref);
        lfSize(end+1:3) = 1;
        imgSize = lfSize(1:3);
        imgRes = lfSize(4:end);
        numPix  = prod(imgSize);
        numView = numel(lfref)/prod(imgSize);
        
        dims = arrayfun(@(x) (1:x)-floor(x/2)-1,lfSize,'UniformOutput',false);
        dims{3} = 1:lfSize(3);
    end
end