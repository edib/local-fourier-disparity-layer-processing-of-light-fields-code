function gv = fun_pre_post_sub_to_gv(lfsize,pre,post,sub)
%FUN_PRE_POST_SUB_TO_GV Summary of this function goes here
%   Detailed explanation goes here

gv = arrayfun(@(x) 1:x,lfsize,'UniformOutput',false);

pre  = num2cell(pre);
post = num2cell(post);
sub  = num2cell(sub);

gv = cellfun(@(dim,pre,post) dim(1+pre:end-post),gv,pre,post,'UniformOutput',false);
gv = cellfun(@(dim,sub) dim(1:sub:end) ,gv,sub,'UniformOutput',false);

end