function [map,nbBits] = fun_decode_map(map_dir,map_name)
%FUN_DECODE_MAP Summary of this function goes here
%   Detailed explanation goes here

name_flif = [map_name,'.flif'   ];
name_png  = [map_name,'_out.png'];

name_flif = fullfile(map_dir,name_flif);
name_png  = fullfile(map_dir,name_png );

nbBits = FLIF.codec('encode',false,'decode',true,...
    'flif',name_flif,'output',name_png,...
    'decode_options','-ovvv -q100');

map = double(imread(name_png));

end