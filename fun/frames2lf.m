function [lf,imgSize,imgRes] = frames2lf(frames)
%FRAMES2LF Summary of this function goes here
%   Detailed explanation goes here

imgSize = size(frames{1});
imgRes = size(frames);

lf = reshape(frames,[1,1,1,size(frames)]);
lf = cell2mat(lf);
end