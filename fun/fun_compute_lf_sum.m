function [lfcol,lflab,lfdims,lfwgt] = fun_compute_lf_sum(srcol,srmask,srdims,srwgt)
%FUN_COMPUTE_LF_SUM Summary of this function goes here
%   Detailed explanation goes here

%%
if ~exist('srwgt','var')
    srwgt = cellfun(@double,srmask,'UniformOutput',false);
end

%%
srxgvs = cellfun(@(srdims) srdims{1},srdims,'UniformOutput',false);
srygvs = cellfun(@(srdims) srdims{2},srdims,'UniformOutput',false);
srcgvs = cellfun(@(srdims) srdims{3},srdims,'UniformOutput',false);
srugvs = cellfun(@(srdims) srdims{4},srdims,'UniformOutput',false);
srvgvs = cellfun(@(srdims) srdims{5},srdims,'UniformOutput',false);

%%
srxmin = min(cellfun(@min,srxgvs)); srxmax = max(cellfun(@max,srxgvs));
srymin = min(cellfun(@min,srygvs)); srymax = max(cellfun(@max,srygvs));
srcmin = min(cellfun(@min,srcgvs)); srcmax = max(cellfun(@max,srcgvs));
srumin = min(cellfun(@min,srugvs)); srumax = max(cellfun(@max,srugvs));
srvmin = min(cellfun(@min,srvgvs)); srvmax = max(cellfun(@max,srvgvs));

%%
lfxgv = srxmin:srxmax;
lfygv = srymin:srymax;
lfcgv = srcmin:srcmax;
lfugv = srumin:srumax;
lfvgv = srvmin:srvmax;

lfdims = {lfxgv,lfygv,lfcgv,lfugv,lfvgv};

lfsizecol = cellfun(@numel,{lfxgv,lfygv,lfcgv,lfugv,lfvgv});
lfsizelab = cellfun(@numel,{lfxgv,lfygv,  1  ,lfugv,lfvgv});

%%
[~,lfxind,srxind] = cellfun(@(srxgv) intersect(lfxgv,srxgv,'stable'),srxgvs,'UniformOutput',false);
[~,lfyind,sryind] = cellfun(@(srygv) intersect(lfygv,srygv,'stable'),srygvs,'UniformOutput',false);
[~,lfcind,srcind] = cellfun(@(srcgv) intersect(lfcgv,srcgv,'stable'),srcgvs,'UniformOutput',false);
[~,lfuind,sruind] = cellfun(@(srugv) intersect(lfugv,srugv,'stable'),srugvs,'UniformOutput',false);
[~,lfvind,srvind] = cellfun(@(srvgv) intersect(lfvgv,srvgv,'stable'),srvgvs,'UniformOutput',false);

%%
lfcol = zeros(lfsizecol);
lflab = zeros(lfsizelab);
lfwgt = zeros(lfsizelab);
numlab = numel(srcol);

%%
for lab = 1:numlab
    srmask_ = srmask{lab};
    srmask_ = any(srmask_,3);
    
    srtemp = lflab(lfxind{lab},lfyind{lab},1,lfuind{lab},lfvind{lab});
    srtemp(srmask_) = lab;
    lflab(lfxind{lab},lfyind{lab},1,lfuind{lab},lfvind{lab}) = srtemp;
    
    srwgttemp = srwgt{lab}(srxind{lab},sryind{lab},1,sruind{lab},srvind{lab});
    lfwgt(lfxind{lab},lfyind{lab},1,lfuind{lab},lfvind{lab}) = ...
        lfwgt(lfxind{lab},lfyind{lab},1,lfuind{lab},lfvind{lab}) + ...
        srwgttemp;
    
    srmask_ = repmat(srmask_,1,1,numel(lfcgv));
    srmask_ = srmask_(srxind{lab},sryind{lab},srcind{lab},sruind{lab},srvind{lab});
    
    srtemp = lfcol(lfxind{lab},lfyind{lab},lfcind{lab},lfuind{lab},lfvind{lab});
    srtemp( srmask_) = srcol{lab}(srmask_);
    srtemp(~srmask_) = 0;
    
    lfcol(lfxind{lab},lfyind{lab},lfcind{lab},lfuind{lab},lfvind{lab}) = ...
        lfcol(lfxind{lab},lfyind{lab},lfcind{lab},lfuind{lab},lfvind{lab}) + ...
        srwgttemp.*srtemp;
end

lfcol = lfcol./lfwgt;

end