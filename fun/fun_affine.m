function [lfreccol,lfrecmask,B,C,DMax,D,metricOutMax,metricInMax] = fun_affine(...
    lfrefcol,lfrefmask,dims,varargin)
%FUN_AFFINE Summary of this function goes here
%   Detailed explanation goes here

metric = 'psnr';
model = 'affine';
coupled = false;
DMax = [0,0,0,0,0,0];
D = DMax;

metricOutMax = -inf;
metricInMax  = -inf;

%%
p = inputParser; p.KeepUnmatched = true; p.StructExpand = true;

p.addParameter('k'           , 1           , @isnumeric);
p.addParameter('numiter'     , 30          , @isnumeric);
p.addParameter('step'        , 1           , @isnumeric);
p.addParameter('metric'      , metric      , @ischar);
p.addParameter('update'      , 1           , @isnumeric);
p.addParameter('completion'  , 1           , @isnumeric);
p.addParameter('maxout'      , true        , @islogical);
p.addParameter('verbose'     , true        , @islogical);
p.addParameter('display'     , false       , @islogical);
p.addParameter('model'       , model       , @ischar);
p.addParameter('coupled'     , coupled     , @islogical);

p.addParameter('DMax'        , DMax        , @(x) isnumeric(x)||iscell(x));
p.addParameter('D'           , D           , @(x) isnumeric(x)||iscell(x));
p.addParameter('metricOutMax', metricOutMax, @(x) isnumeric(x)||iscell(x));
p.addParameter('metricInMax' , metricInMax , @(x) isnumeric(x)||iscell(x));

p.addParameter('sequential'  , false       , @islogical);

p.parse(varargin{:});

k            = p.Results.k;
numiter      = p.Results.numiter;
metric       = p.Results.metric;
maxout       = p.Results.maxout;
verbose      = p.Results.verbose;
show         = p.Results.display;
step         = p.Results.step;
update       = p.Results.update;
completion   = p.Results.completion;
model        = p.Results.model;
coupled      = p.Results.coupled;

DMax         = p.Results.DMax;
D            = p.Results.D;
metricOutMax = p.Results.metricOutMax;
metricInMax  = p.Results.metricInMax;

sequential   = p.Results.sequential;

fun = @(lfrefcol,lfrefmask,dims,DMax,D,metricOutMax,metricInMax)...
    fun_affine_aux(lfrefcol,lfrefmask,dims,DMax,D,metricOutMax,metricInMax,...
    k,numiter,metric,maxout,verbose,show,step,update,completion,model,coupled);

if iscell(lfrefcol)
    numlab = numel(lfrefcol);
    labels = find(~cellfun(@isempty,lfrefcol));
    
    p.addParameter('labels',labels,@isnumeric);
    p.parse(varargin{:});
    labels = p.Results.labels;
    
    if sequential
        [lfreccol,lfrecmask,B,C] = deal(cell(size(lfrefcol)));
        
        for lab = 1:numlab
            if ~any(lab==labels); continue; end
            
            [lfreccol{lab},lfrecmask{lab},B{lab},C{lab},DMax{lab},D{lab},...
                metricOutMax{lab},metricInMax{lab}] = fun(...
                lfrefcol{lab},lfrefmask{lab},dims{lab},...
                DMax{lab},D{lab},metricOutMax{lab},metricInMax{lab});
        end
    else
        parfor lab = 1:numlab
            if ~any(lab==labels); continue; end
            
            [lfreccol{lab},lfrecmask{lab},B{lab},C{lab},DMax{lab},D{lab},...
                metricOutMax{lab},metricInMax{lab}] = fun(...
                lfrefcol{lab},lfrefmask{lab},dims{lab},...
                DMax{lab},D{lab},metricOutMax{lab},metricInMax{lab});
        end
    end
else
    [lfreccol,lfrecmask,B,C,DMax,D,metricOutMax,metricInMax] = fun(...
        lfrefcol,lfrefmask,dims,DMax,D,metricOutMax,metricInMax);
end

    function [lfreccol,lfrecmask,B,C,DMax,D,metricOutMax,metricInMax] = ...
        fun_affine_aux(lfrefcol,lfrefmask,dims,DMax,D,metricOutMax,metricInMax,...
        k,numIter,metric,maxOut,verbose,show,step,update,completion,model,coupled)
    
    iterMax = 1;
    
    [lfSize,imgSize,imgRes,numPix,numView] = get_dims(lfrefcol);
    lfrefmask = lfrefmask.*ones(lfSize);
    
    %%
    TMax  = fun_affine_to_homography( DMax,imgRes);
    TiMax = fun_affine_to_homography(-DMax,imgRes);
    T     = fun_affine_to_homography( D   ,imgRes);
    Ti    = fun_affine_to_homography(-D   ,imgRes);
    
    %%
    metricOut = nan(1,numIter);
    metricIn  = nan(1,numIter);
    
    %%
    switch lower(metric)
        case 'psnr'
            metric = @psnr;
            mstr = 'PSNR';
            unitstr = 'dB';
        case 'ssim'
            metric = @ssim;
            mstr = 'SSIM';
            unitstr = '';
    end
    
    metricTitle = [mstr,' (',unitstr,')'];
    
    %%
    bSize = [imgSize,k];
    mSize = [numPix ,numView];
    
    %% Start chronometer
    tStart = tic;
    fprintf('Low-rank disparity parameters computation started\n');
    
    %%
    if coupled; coupledstr = 'coupled'; else; coupledstr = 'non coupled'; end
    fprintf(['Model: ',model,', ',coupledstr,'\n']);
    
    %%
    for iter = 1:numIter
        % forward warping
        [M,warpmask] = fun_warp_homography(lfrefcol,lfrefmask,T,dims);
        
        % low-rank approximation
        [B,C,M] = fun_factorize(M,warpmask,k,'colDims',[1,2,3],'resize',false);
        
        % mask completion
        compmask = reshape(logical(warpmask),mSize);
        
        if (completion==0); compmask = true(size(compmask)); end
        %if(completion==1); compmask = compmask; end
        if (completion==2); compmask = repmat(any(compmask,2),1,size(compmask,2)); end
        if (completion==3); compmask = repmat(all(compmask,2),1,size(compmask,2)); end
        
        compmask = reshape(double(compmask),lfSize);
        
        M  = reshape(M  ,lfSize);
        BC = reshape(B*C,lfSize);
        metricIn(iter) = mean_metric(BC,M,compmask);
        
        % inverse warping
        [lfreccol,lfrecmask] = fun_warp_homography(BC,compmask,Ti,dims);
        metricOut(iter) = mean_metric(lfreccol,lfrefcol,~isnan(lfrefcol)&lfrecmask);
        
        update_max_transform;
        
        if verbose; display_progress; end
        if show; display_graph; end
        
        % transform update
        D = fun_update_affine(M,warpmask,D,BC-M,step,update,dims,model,coupled);
        T  = fun_affine_to_homography( D,imgRes);
        Ti = fun_affine_to_homography(-D,imgRes);
    end
    
    %%
    % forward warping
    [M,lfrecmask] = fun_warp_homography(lfrefcol,lfrefmask,TMax,dims);
    
    % low-rank approximation
    [B,C] = fun_factorize(M,lfrecmask,k,'colDims',[1,2,3],'resize',false);
    
    % mask completion
    lfrecmask = reshape(logical(lfrecmask),mSize);
    
    if (completion==0); lfrecmask = true(size(lfrecmask)); end
    %if(completion==1); masklfrec = masklfrec; end
    if (completion==2); lfrecmask = repmat(any(lfrecmask,2),1,size(lfrecmask,2)); end
    if (completion==3); lfrecmask = repmat(all(lfrecmask,2),1,size(lfrecmask,2)); end
    
    lfrecmask = reshape(double(lfrecmask),lfSize);
    
    % inverse warping
    BC = reshape(B*C,lfSize);
    B  = reshape(B,bSize);
    [lfreccol,lfrecmask] = fun_warp_homography(BC,lfrecmask,TiMax,dims);
    
    lfreccol(~lfrecmask) = nan;
    
    %% Stop chronometer
    tEnd = toc(tStart);
    fprintf(['Low-rank disparity parameters computation finished after ',num2str(tEnd),' seconds\n']);
    
    %% Auxiliary functions
    
    function res = mean_metric(lfrec,lfref,mask)
        lfrec  = lf2frames(lfrec);
        lfref  = lf2frames(lfref);
        mask = lf2frames(logical(mask));
        
        res = cellfun(@(lfrec,lfref,mask) metric(lfrec(mask),lfref(mask)),lfrec,lfref,mask);
        res = mean(res(:));
    end
    
    function update_max_transform()
        if (maxOut && metricOut(iter)>metricOutMax)...
                || (~maxOut && metricIn(iter)>metricInMax)
            iterMax = iter;
            metricOutMax = metricOut(iter);
            metricInMax = metricIn(iter);
            TMax = T;
            TiMax = inv_T(TMax);
            DMax = D;
        end
    end
    
    function display_progress()
        fprintf(['iter: %4d, ',mstr,'out: %3.2f ',unitstr,', ',mstr,'in: %3.2f ',unitstr,'\n'...
            'max : %4d, ',mstr,'out: %3.2f ',unitstr,', ',mstr,'in: %3.2f ',unitstr,'\n'],...
            iter,metricOut(iter),metricIn(iter),...
            iterMax,metricOutMax,metricInMax);
    end
    
    function display_graph()
        persistent ax
        
        if  isempty(ax)||~isvalid(ax); figure; ax = gca; end
        
        hold(ax,'off')
        ax.ColorOrderIndex = 1;
        plot(ax,metricIn,'DisplayName',[mstr,'in']);
        hold(ax,'on')
        plot(ax,metricOut,'DisplayName',[mstr,'out']);
        ax.ColorOrderIndex = 1;
        scatter(ax,iterMax,metricInMax,'+','DisplayName',[mstr,'in max']);
        scatter(ax,iterMax,metricOutMax,'+','DisplayName',[mstr,'out max']);
        
        grid(ax,'on')
        grid(ax,'minor')
        xlabel(ax,'iterations');
        ylabel(ax,metricTitle);
        title(ax,metricTitle);
        legend(ax,'show','Location','nw');
        
        drawnow
    end
    
    function T = inv_T(T)
        T = reshape(T,3,3,[]);
        
        for it = 1:size(T,3)
            T(:,:,it) = inv(T(:,:,it));
        end
        
        T = reshape(T,9,[]);
    end
    
    function [lfSize,imgSize,imgRes,numPix,numView,dims] = get_dims(lfref)
        lfSize = size(lfref);
        lfSize(end+1:3) = 1;
        imgSize = lfSize(1:3);
        imgRes = lfSize(4:end);
        numPix  = prod(imgSize);
        numView = numel(lfref)/prod(imgSize);
        
        dims = arrayfun(@(x) (1:x)-floor(x/2)-1,lfSize,'UniformOutput',false);
        dims{3} = 1:lfSize(3);
    end
end

end