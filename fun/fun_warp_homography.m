function [I,mask] = fun_warp_homography(I,mask,T,dims_,method,extrapval)
%FUN_WARP_HOMOGRAPHY Summary of this function goes here
%   Detailed explanation goes here

[~,imgSize,~,~,numView,defdims] = get_dims(I);
numChan = imgSize(3);
numMask = size(mask,3);

if ~exist('dims_','var'); dims_ = defdims; end
if ~exist('method','var'); method = 'cubic'; end
if ~exist('extrapval','var'); extrapval = nan; end

seh = strel('arbitrary',[1,0,1]);
sev = strel('arbitrary',[1,0,1]');

persistent XYO X Y dims

update = isempty(dims) || ~isequal(dims_,dims);

if update
    dims = dims_;
    
    [X,Y] = ndgrid(dims{1:2});
    O = ones(imgSize(1:2));
    
    XYO = [X(:),Y(:),O(:)];
end

while (any(isnan(I(:))))
    I = fun_fillmissing(I,seh,1);
    I = fun_fillmissing(I,sev,1);
end

mask = double(mask);
mask(isnan(mask)) = 0;

for v = 1:numView
    UVO = XYO*reshape(T(:,v),[3,3]);
    UVO = reshape(UVO./UVO(:,3),[imgSize(1:2),3]);
    
    for c = 1:numChan
        I   (:,:,c,v) = interp2(Y,X,I   (:,:,c,v),UVO(:,:,2),UVO(:,:,1),method,extrapval);
    end
    
    for c = 1:numMask
        mask(:,:,c,v) = interp2(Y,X,mask(:,:,c,v),UVO(:,:,2),UVO(:,:,1),'nearest',0);
    end
end

    function [lfSize,imgSize,imgRes,numPix,numView,dims] = get_dims(lfref)
        lfSize = size(lfref);
        lfSize(end+1:3) = 1;
        imgSize = lfSize(1:3);
        imgRes = lfSize(4:end);
        numPix  = prod(imgSize);
        numView = numel(lfref)/prod(imgSize);
        
        dims = arrayfun(@(x) (1:x)-floor(x/2)-1,lfSize,'UniformOutput',false);
    end
end