function fun_compute_quality_metrics(resuDir,LFName,QPList)
%ENCODEFDL Summary of this function goes here
%   Detailed explanation goes here

tStart = tic;

%% Load reference
refDir = fullfile(resuDir,'Reference');
LFRef = LF.fromSubAps(refDir,'ext','png','name',LFName);

%% Compute quality metrics for each QP value
for QP = QPList
    %% Current directories
    predDir     = fullfile(resuDir,['Prediction_',num2str(QP)]);
    recDir      = fullfile(resuDir,['Reconstruction_',num2str(QP)]);
    resuMatPred = fullfile(resuDir,['quality_metrics_pred_',num2str(QP),'.mat']);
    resuMatRec  = fullfile(resuDir,['quality_metrics_rec_',num2str(QP),'.mat']);
    
    %% Prediction of current frame layer using Fourier Disparity Layers
    LFRec = LF.fromSubAps(predDir,'name',LFName,'ext','png');
    [psnr_y_view,psnr_y_mean,psnr_y_all] = LF.qualityMetrics(LFRec,LFRef);
    save(resuMatPred,'psnr_y_view','psnr_y_mean','psnr_y_all');
    
    if(~isnan(QP))
        LFRec = LF.fromSubAps(recDir,'name',LFName,'ext','png');
        [psnr_y_view,psnr_y_mean,psnr_y_all] = LF.qualityMetrics(LFRec,LFRef);
        save(resuMatRec,'psnr_y_view','psnr_y_mean','psnr_y_all');
    end
end

toc(tStart)

end