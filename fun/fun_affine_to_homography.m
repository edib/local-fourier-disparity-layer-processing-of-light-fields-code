function [T,Ti] = fun_affine_to_homography(D,imgRes)
%FUN_AFFINE_TO_HOMOGRAPHY Summary of this function goes here
%   Detailed explanation goes here

imgResc = floor(imgRes/2)+1;
numView = prod(imgRes);

T = zeros(9,numView);

[U,V] = ndgrid(1:imgRes(1),1:imgRes(2));
U = reshape(U,1,[]);
V = reshape(V,1,[]);
U = U - imgResc(1);
V = V - imgResc(2);

T(1,:) = 1+D(1).*U;
T(2,:) =   D(2).*U;
T(3,:) =   D(3).*U;
T(4,:) =   D(4).*V;
T(5,:) = 1+D(5).*V;
T(6,:) =   D(6).*V;
%T(7,:) = 0;
%T(8,:) = 0;
T(9,:) = 1;

den = 1 + U.*D(1) + V.*D(2);

Ti(1,:) = 1-D(1).*U./den;
Ti(2,:) =  -D(2).*U./den;
Ti(3,:) =  -D(3).*U./den;
Ti(4,:) =  -D(4).*V./den;
Ti(5,:) = 1-D(5).*V./den;
Ti(6,:) =  -D(6).*V./den;
%Ti(7,:) = 0;
%Ti(8,:) = 0;
Ti(9,:) = 1;

end