function [color,label,dims] = fun_read_color_label_dir(coldirname,labdirname)
%FUN_READ_COLOR_LABEL_DIR Summary of this function goes here
%   Detailed explanation goes here

%% Read color and label from directories
color = fun_read_subapertures(coldirname,'name','','ext','png');
label = fun_read_subapertures(labdirname,'name','','ext','png');

color = double(color)./double(intmax(class(color)));
label = double(label);

sz = size(color);
sz(end+1:5) = 1;
dims = arrayfun(@(x) 1:x,sz,'UniformOutput',false);
dims{4} = dims{4} - floor(numel(dims{4})/2) - 1;
dims{5} = dims{5} - floor(numel(dims{5})/2) - 1;

end