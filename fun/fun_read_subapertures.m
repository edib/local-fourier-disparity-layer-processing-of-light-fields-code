function [lf,frames,cmap,alpha] = fun_read_subapertures(dirname,varargin)
%FUN_READ_SUBAPERTURES Summary of this function goes here
%   Detailed explanation goes here

% Parse input parameters
p = inputParser();
p.addParameter('name' , ''    , @ischar);
p.addParameter('ext'  , '.png', @ischar);
p.addParameter('shift', 0     , @isnumeric);
p.parse(varargin{:});

shift = p.Results.shift;
name  = p.Results.name;
ext   = p.Results.ext;

if ~startsWith(ext,'.')
    ext = ['.',ext];
end

% List files in directory
listing = dir(fullfile(dirname,[name,'*',ext]));

% Select files corresponding to given name and extension
[~,filenames,~] = cellfun(@fileparts,{listing.name},'UniformOutput',false);

% Separate filename into tokens following name_u_v convention, u and v are
% angular positions

if isempty(name)
    inds_u_v = cellfun(@(filename) regexp(filename,'(\d*)_(\d*)','tokens'),...
        filenames,'UniformOutput',false);
else
    inds_u_v = cellfun(@(filename) regexp(filename,['^',name,'_(\d*)_(\d*)'],'tokens'),...
        filenames,'UniformOutput',false);
end

% Remove empty cells
empty = cellfun(@isempty,inds_u_v);
inds_u_v = inds_u_v(~empty);
filenames = filenames(~empty);

inds_u_v = cellfun(@(ind_uv) ind_uv{:},inds_u_v,'UniformOutput',false);

% Default output
cmap  = [];
alpha = [];

frames = {};
cmap = {};
alpha = {};

% Read frames and assemble them together according to index
cellfun(@fill_frames,filenames,inds_u_v);

% Fill any missing frame
frames = fill_empty(frames);

% Join frames
lf = frames2lf(frames);

%%
    function fill_frames(filename,ind_u_v)
        fullname = fullfile(dirname,[filename,ext]);
        
        uv = str2double(ind_u_v);
        uv = uv-shift;
        
        switch ext
            case {'.png'}
                [frame,map,alph] = imread(fullname);
                frames      {uv(1),uv(2)} = frame;
                cmap {uv(1),uv(2)} = map;
                alpha{uv(1),uv(2)} = alph;
            otherwise
                frame = imread(fullname);
                frames{uv(1),uv(2)} = frame;
        end
    end

    function frames = fill_empty(frames)
        ind_empty = cellfun(@isempty,frames);
        
        if all(ind_empty(:))
            frames = [];
        elseif any(ind_empty(:))
            non_empty_frames = frames(~ind_empty);
            warning('Some (but not all) frames were missing, filling with zeros');
            
            frames_size = cellfun(@size,non_empty_frames,'UniformOutput',false);
            
            if ~isequal(frames_size{:})
                error('Inconsistent size from captured frames');
            else
                replacement = zeros(frames_size{1},'like',non_empty_frames{1});
                frames(ind_empty) = deal({replacement});
            end
        end
    end
end