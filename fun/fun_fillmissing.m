function Value = fun_fillmissing(Value,se,numIter)
%FUN_FILLMISSING Extend signal using structuring element se numIter times
%   Detailed explanation goes here

if ~exist('numIter','var'); numIter = +inf; end
if ~iscell(se); se = {se}; end

Mask = isnan(Value);

it = 0;
nnzdiff = nnz(Mask(:));
nnzcur = nnzdiff;

while nnzdiff && it<numIter
    Temp = Value;
    Temp(Mask)=-Inf;
    
    for it = 1:numel(se)
        Temp = imdilate(Temp,se{it});
    end
    
    Temp(isinf(Temp))=NaN;
    Value(Mask) = Temp(Mask);
    
    Mask = isnan(Value);
    
    it = it+1;
    nnzdiff = nnzcur -nnz(Mask(:));
    nnzcur = nnz(Mask(:));
end

end