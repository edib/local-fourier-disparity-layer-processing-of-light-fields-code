function fun_affine_sr_dir(refdir,recdir,kList,varargin)
%FUN_AFFINE_SR_DIR Summary of this function goes here
%   Detailed explanation goes here

if ~exist(recdir,'dir'); mkdir(recdir); end

if isdeployed()
    kList = eval(kList);
    varargin(2:2:end) = cellfun(@eval,varargin(2:2:end),'UniformOutput',false);
end

%% Parse input
p = inputParser; p.KeepUnmatched = true; p.StructExpand = true;

p.addParameter('pause'     , 0       , @isnumeric);
p.addParameter('load'      , true    , @islogical);
p.addParameter('reset'     , true    , @islogical);
p.addParameter('overwrite' , false   , @islogical);
p.addParameter('additional', false   , @islogical);

p.addParameter('model'     , 'affine', @ischar   );
p.addParameter('coupled'   , false   , @islogical);

p.parse(varargin{:});

pause_     = p.Results.pause;
load_      = p.Results.load;
reset_     = p.Results.reset;
overwrite  = p.Results.overwrite;
additional = p.Results.additional;

model      = p.Results.model;
coupled    = p.Results.coupled;

%%
if coupled; coupled = 'coupled'; else; coupled = 'non_coupled'; end

fprintf(['Pausing ',num2str(pause_),' seconds\n']);
pause(pause_);

%% Load/compute super-rays
if load_
    try
        srfile = fullfile(refdir,'super_rays.mat');
        mfile = matfile(srfile,'Writable',false);
        
        srrefcol  = mfile.color;
        srrefmask = mfile.mask;
        srrefdims = mfile.dims;
    catch
        [srrefcol,srrefmask,srrefdims] = fun_compute_sr_dir(refdir,recdir,varargin{:});
    end
else
    [srrefcol,srrefmask,srrefdims] = fun_compute_sr_dir(refdir,recdir,varargin{:});
end

%% Initialize variables
[srreccol,srrecmask,B,C,DMax,D,metricOutMax,metricInMax] = deal(cell(size(srrefcol)));
reset_params();

%%
for k = kList
    %% Create results file
    resfile = ['disparity_parameters_',model,'_',coupled,'_',num2str(k),'.mat'];
    resfile = fullfile(recdir,resfile);
    
    %% Load disparity parameters if they exist and if necessary
    if reset_
        reset_params();
    elseif exist(resfile,'file')
        try
            load_params();
        catch
            reset_params();
        end
    end
    
    %% Compute disparity parameters if non existing or if necessary
    if ~exist(resfile,'file') || overwrite
        %% Optimize affine disparity parameters
        [srreccol,srrecmask,B,C,DMax,D,metricOutMax,metricInMax] = ...
            fun_affine(srrefcol,srrefmask,srrefdims,varargin{:},...
            'k',k,'DMax',DMax,'D',D,...
            'metricOutMax',metricOutMax,'metricInMax',metricInMax);
        
        %% Save Results
        save_params();
    end
end

    function reset_params()
        [DMax{:}] = deal([0,0,0,0,0,0]);
        [D{:}] = deal([0,0,0,0,0,0]);
        [metricOutMax{:}] = deal(-inf);
        [metricInMax{:}] = deal(-inf);
    end

    function load_params()
        m = matfile(resfile,'Writable',false);
        DMax = m.DMax;
        D = m.D;
        metricOutMax = m.metricOutMax;
        metricInMax = m.metricInMax;
    end

    function save_params()
        m = matfile(resfile,'Writable',true);
        
        m.DMax = DMax;
        m.D = D;
        m.metricOutMax = metricOutMax;
        m.metricInMax = metricInMax;
        
        if additional
            m.srreccol = srreccol;
            m.srrecmask = srrecmask;
            m.B = B;
            m.C = C;
        end
    end
end