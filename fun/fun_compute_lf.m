function [lfrefcol,lfreflab,lfrefdims] = fun_compute_lf(srrefcol,srmask,srdims)
%FUN_COMPUTE_LF Summary of this function goes here
%   Detailed explanation goes here

%%
srxgvs = cellfun(@(srdims) srdims{1},srdims,'UniformOutput',false);
srygvs = cellfun(@(srdims) srdims{2},srdims,'UniformOutput',false);
srcgvs = cellfun(@(srdims) srdims{3},srdims,'UniformOutput',false);
srugvs = cellfun(@(srdims) srdims{4},srdims,'UniformOutput',false);
srvgvs = cellfun(@(srdims) srdims{5},srdims,'UniformOutput',false);

%%
srxmin = min(cellfun(@min,srxgvs)); srxmax = max(cellfun(@max,srxgvs));
srymin = min(cellfun(@min,srygvs)); srymax = max(cellfun(@max,srygvs));
srcmin = min(cellfun(@min,srcgvs)); srcmax = max(cellfun(@max,srcgvs));
srumin = min(cellfun(@min,srugvs)); srumax = max(cellfun(@max,srugvs));
srvmin = min(cellfun(@min,srvgvs)); srvmax = max(cellfun(@max,srvgvs));

%%
lfxgv = srxmin:srxmax;
lfygv = srymin:srymax;
lfcgv = srcmin:srcmax;
lfugv = srumin:srumax;
lfvgv = srvmin:srvmax;

lfrefdims = {lfxgv,lfygv,lfcgv,lfugv,lfvgv};

lfsizecol = cellfun(@numel,{lfxgv,lfygv,lfcgv,lfugv,lfvgv});
lfsizelab = cellfun(@numel,{lfxgv,lfygv,  1  ,lfugv,lfvgv});

%%
[~,lfxind,srxind] = cellfun(@(srxgv) intersect(lfxgv,srxgv,'stable'),srxgvs,'UniformOutput',false);
[~,lfyind,sryind] = cellfun(@(srygv) intersect(lfygv,srygv,'stable'),srygvs,'UniformOutput',false);
[~,lfcind,srcind] = cellfun(@(srcgv) intersect(lfcgv,srcgv,'stable'),srcgvs,'UniformOutput',false);
[~,lfuind,sruind] = cellfun(@(srugv) intersect(lfugv,srugv,'stable'),srugvs,'UniformOutput',false);
[~,lfvind,srvind] = cellfun(@(srvgv) intersect(lfvgv,srvgv,'stable'),srvgvs,'UniformOutput',false);

%%
lfrefcol = nan(lfsizecol);
lfreflab = zeros(lfsizelab);
numlab = numel(srrefcol);

for lab = 1:numlab
    srmask = srmask{lab};
    srmask = any(srmask,3);

    srreftemp = lfreflab(lfxind{lab},lfyind{lab},1,lfuind{lab},lfvind{lab});
    srreftemp(srmask) = lab;
    lfreflab(lfxind{lab},lfyind{lab},1,lfuind{lab},lfvind{lab}) = srreftemp;

    %srmask = srrefmask{lab};
    srmask = repmat(srmask,1,1,numel(lfcgv));
    
    srreftemp = lfrefcol(lfxind{lab},lfyind{lab},lfcind{lab},lfuind{lab},lfvind{lab});
    srmask = srmask(srxind{lab},sryind{lab},srcind{lab},sruind{lab},srvind{lab});
    srreftemp(srmask) = srrefcol{lab}(srmask);
    lfrefcol(lfxind{lab},lfyind{lab},lfcind{lab},lfuind{lab},lfvind{lab}) = srreftemp;
end

end