function [M,mask] = fun_complete(M,mask,method,varargin)
%FUN_COMPLETE Summary of this function goes here
%   Detailed explanation goes here

maxIter=500;
switch method
    case 'trace'
        alpha=[1,1]; betaMult=1.1 ; epsilon=1e-3; TraceNormToRankParam=0;
    case 'rank'
        alpha=[1,1]; betaMult=1.44; epsilon=1e-3; TraceNormToRankParam=inf;
    otherwise
        error('Unknown completion method');
end

p = inputParser; p.StructExpand = true; p.KeepUnmatched = true;
p.addParameter('alpha'               , alpha               , @isnumeric);
p.addParameter('betaMult'            , betaMult            , @isnumeric);
p.addParameter('maxIter'             , maxIter             , @isnumeric);
p.addParameter('epsilon'             , epsilon             , @isnumeric);
p.addParameter('TraceNormToRankParam', TraceNormToRankParam, @isnumeric);

p.parse(varargin{:});

alpha                = p.Results.alpha;
betaMult             = p.Results.betaMult;
maxIter              = p.Results.maxIter;
epsilon              = p.Results.epsilon;
TraceNormToRankParam = p.Results.TraceNormToRankParam;

p.addParameter('colDims', []  , @isnumeric);
p.addParameter('rowDims', []  , @isnumeric);
p.addParameter('resize' , true, @islogical);

p.parse(varargin{:});

colDims = p.Results.colDims;
rowDims = p.Results.rowDims;
resize  = p.Results.resize;

numDims = ndims(M);
if (isempty(colDims)&&isempty(rowDims)); colDims = 1:min(3,numDims-1); end
if isempty(colDims); colDims = setdiff(1:numDims,rowDims); end
if isempty(rowDims); rowDims = setdiff(1:numDims,colDims); end

colSize = arrayfun(@(dim) size(M,dim),colDims);
rowSize = arrayfun(@(dim) size(M,dim),rowDims);

singdims = find(size(M)./size(mask)~=1);

mask = logical(mask) | false(size(M));

M = permute(M,[colDims,rowDims]);
M = reshape(M,[prod(colSize),prod(rowSize)]);
mask = permute(mask,[colDims,rowDims]);
mask = reshape(mask,[prod(colSize),prod(rowSize)]);

mask(isnan(M)) = false;
M(~mask) = 0;

M = LRTCADMrho(M,mask,alpha,betaMult,maxIter,epsilon,TraceNormToRankParam);

mask = any(mask,2);
M(~mask,:) = NaN;
mask = mask | false(size(M));

if resize
    M = reshape(M,[colSize,rowSize]);
    M = ipermute(M,[colDims,rowDims]);
    mask = reshape(mask,[colSize,rowSize]);
    mask = ipermute(mask,[colDims,rowDims]);
    mask = any(mask,singdims);
end
end