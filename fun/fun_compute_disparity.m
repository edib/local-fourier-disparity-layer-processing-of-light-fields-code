function [refdisp_x,refdisp_y,warpdisp_x,warpdisp_y] = fun_compute_disparity(D,srdims)
%FUN_COMPUTE_DISPARITY Summary of this function goes here
%   Detailed explanation goes here

%%
[x,y,~,u,v] = cellfun(@(dims) deal(dims{:}),srdims,'UniformOutput',false);
x = cellfun(@(x) reshape(x,[],1),x,'UniformOutput',false);
y = cellfun(@(x) reshape(x,1,[]),y,'UniformOutput',false);
u = reshape(u{1},1,1,1,[],1);
v = reshape(v{1},1,1,1,1,[]);

%%
[a,b,c,d,e,f] = cellfun(@(d) deal(d(1),d(2),d(3),d(4),d(5),d(6)),D,...
    'UniformOutput',false);

ref_disp  = @(x,y,a,b,c) (a.*x+b.*y+c)./(1+a.*u+b.*v);
warp_disp = @(x,y,a,b,c) (a.*x+b.*y+c).*ones(size(u.*v));

refdisp_x = cellfun(ref_disp,x,y,a,b,c,'UniformOutput',false);
refdisp_y = cellfun(ref_disp,x,y,d,e,f,'UniformOutput',false);

warpdisp_x = cellfun(warp_disp,x,y,a,b,c,'UniformOutput',false);
warpdisp_y = cellfun(warp_disp,x,y,d,e,f,'UniformOutput',false);

end