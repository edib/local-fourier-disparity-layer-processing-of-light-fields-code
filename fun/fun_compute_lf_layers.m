function [lfcol,lflab,lfdisp,lfdims] = ...
    fun_compute_lf_layers(srcol,srmask,srdisp,srdims,numlayers)
%FUN_COMPUTE_LF_LAYERS Summary of this function goes here
%   Detailed explanation goes here

if ~exist('numlayers','var'); numlayers = 1; end

%% Add buffer layer
numlayers = numlayers+1;

%% Split super-ray ranges according to dimensions
srxgvs = cellfun(@(srdims) srdims{1},srdims,'UniformOutput',false);
srygvs = cellfun(@(srdims) srdims{2},srdims,'UniformOutput',false);
srcgv = srdims{1}{3};
srugv = srdims{1}{4};
srvgv = srdims{1}{5};

%% Compute global super-ray spatial range
srxmin = min(cellfun(@min,srxgvs));
srxmax = max(cellfun(@max,srxgvs));
srymin = min(cellfun(@min,srygvs));
srymax = max(cellfun(@max,srygvs));

%% Infer light field range
lfxgv = srxmin:srxmax;
lfygv = srymin:srymax;
lfcgv = srcgv;
lfugv = srugv;
lfvgv = srvgv;
lfdims = {lfxgv,lfygv,lfcgv,lfugv,lfvgv};

lfsizecol = cellfun(@numel,{lfxgv,lfygv,srcgv,srugv,srvgv});
lfsizelab = cellfun(@numel,{lfxgv,lfygv,  1  ,srugv,srvgv});

%% Compute correspondence between light field and super-rays ranges
[~,lfxind,~] = cellfun(@(srxgv) intersect(lfxgv,srxgv,'stable'),srxgvs,'UniformOutput',false);
[~,lfyind,~] = cellfun(@(srygv) intersect(lfygv,srygv,'stable'),srygvs,'UniformOutput',false);
[~,lfcind,~] = intersect(lfcgv,srcgv,'stable');
[~,lfuind,~] = intersect(lfugv,srugv,'stable');
[~,lfvind,~] = intersect(lfvgv,srvgv,'stable');

%% Initialize light field properties
lfcol  = nan  ([lfsizecol,numlayers]);
lflab  = zeros([lfsizelab,numlayers]);
lfdisp = -inf ([lfsizelab,numlayers]);
numlab = numel(srcol);
numcol = numel(srcgv);

%% Merge super-rays into a light field according to disparity
% Inpaint new layer if overlap occurs
for lab = 1:numlab
    % Compute current super-ray mask
    srmask_ = any(srmask{lab},3);
    
    % Sort disparity
    temp = lfdisp(lfxind{lab},lfyind{lab},1,lfuind,lfvind,:);
    
    sz = size(temp);
    temp = reshape(temp,[],numlayers);
    temp(:,end) = reshape(srdisp{lab},[],1);
    [temp(srmask_(:),:),sig] = sort(temp(srmask_(:),:),2,'descend');
    temp = reshape(temp,sz);
    
    lfdisp(lfxind{lab},lfyind{lab},1,lfuind,lfvind,:) = temp;
    
    x = ndgrid(1:size(sig,1),1:size(sig,2));
    sig = sub2ind(size(sig),x,sig);
    
    % Sort labels
    temp = lflab(lfxind{lab},lfyind{lab},1,lfuind,lfvind,:);
    
    temp = reshape(temp,[],numlayers);
    temp(:,end) = reshape(lab.*srmask_,[],1);
    temp_ = temp(srmask_(:),:);
    temp_ = temp_(sig);
    temp(srmask_(:),:) = temp_;
    temp = reshape(temp,sz);
    
    lflab(lfxind{lab},lfyind{lab},1,lfuind,lfvind,:) = temp;
    
    % Sort colors
    for c = 1:numcol
        temp = lfcol(lfxind{lab},lfyind{lab},lfcind(c),lfuind,lfvind,:);
        
        temp = reshape(temp,[],numlayers);
        temp(:,end) = reshape(srcol{lab}(:,:,c,:,:),[],1);
        temp_ = temp(srmask_(:),:);
        temp_ = temp_(sig);
        temp(srmask_(:),:) = temp_;
        temp = reshape(temp,sz);
        
        lfcol(lfxind{lab},lfyind{lab},lfcind(c),lfuind,lfvind,:) = temp;
    end
end

%% Remove buffer layer
lfcol  = lfcol (:,:,:,:,:,1:end-1);
lflab  = lflab (:,:,:,:,:,1:end-1);
lfdisp = lfdisp(:,:,:,:,:,1:end-1);

end