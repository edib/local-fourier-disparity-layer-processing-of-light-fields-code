function [lfb,lfbmean,lfbrange] = fun_encode_decode_eigen_images(lfb,varargin)
%FUN_ENCODE_DECODE_EIGEN_IMAGES Summary of this function goes here
%   Detailed explanation goes here

%%
[lfbmin,lfbmax] = bounds(lfb(:,:,:,:,1),'all','omitnan');
lfbmean = (lfbmin+lfbmax)/2;

%%
lfb(:,:,:,:,1) = lfb(:,:,:,:,1)-lfbmean;

%%
[lfb,lfbrange] = utils.normalize(lfb);
lfb = utils.recast(lfb,'uint16');

%%
lfb = LF.codec(lfb,varargin{:});

%%
lfb = utils.recast(lfb,'double');
lfb = utils.rescale(lfb,lfbrange);

%%
lfb(:,:,:,:,1) = lfb(:,:,:,:,1)+lfbmean;

end