function [lf,dims,gv] = fun_crop_subsamp(lf,pre,post,sub,dims)
%FUN_CROP_SUBSAMP Summary of this function goes here
%   Detailed explanation goes here

if ~exist('dims','var')
    dims = arrayfun(@(x) 1:x,size(lf),'UniformOutput',false);
end

gv = fun_pre_post_sub_to_gv(size(lf),pre,post,sub);

lf = lf(gv{:});
dims = cellfun(@(dim,gv) dim(gv),dims,gv,'UniformOutput',false);

end