function [color,mask,dims] = fun_compute_sr_dir(refdir,recdir,varargin)
%FUN_COMPUTE_SR_DIR Summary of this function goes here
%   Detailed explanation goes here

if ~exist(recdir,'dir'); mkdir(recdir); end

%% Parse input
p = inputParser; p.KeepUnmatched = true; p.StructExpand = true;

p.addParameter('save'       , false , @islogical);
p.addParameter('colorsubdir','Y'    , @ischar);
p.addParameter('labelsubdir','Label', @ischar);

p.parse(varargin{:});

save_       = p.Results.save;
colorsubdir = p.Results.colorsubdir;
labelsubdir = p.Results.labelsubdir;

%% Load light field (color and label)
colorsubdir = fullfile(refdir,colorsubdir);
labelsubdir = fullfile(refdir,labelsubdir);

[color,label,dims] = fun_read_color_label_dir(colorsubdir,labelsubdir,varargin{:});

%% Compute super-rays
[color,mask,dims] = fun_compute_sr(color,label,dims,varargin{:});

%% Create results file
if save_
    resfile = 'super_rays.mat';
    resfile = fullfile(refdir,resfile);
    
    fprintf('Saving super-rays\n');
    save(resfile,'color','mask','dims');
end

end