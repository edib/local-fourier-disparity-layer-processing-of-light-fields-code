%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% ADM algorithm: tensor completion
% paper: Tensor completion for estimating missing values in visual data
% date: 05-22-2011
% min_X: \sum_i \alpha_i \|X_(i)\|_*
% s.t.:  X_\Omega = T_\Omega
%Note MLP : corresponds to algorithm 4 (HaLRTC) in the article
%(i.e. using augmented lagrangian method and alternating direction).
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function [X_final, X, M, rank, errList,OmegaPrime] = LRTCADMrho(T, Omega, alpha, betaMult, maxIter, epsilon, TraceNormToRankParam, CorruptCols, maxRank, X)

legacyThreshold = false; %true  : threshold value determined for nuclear norm minimization.
                         %false : threshold value determined for rank minimization.
                         %This parameter is automatically set to true when TraceNormToRankParam=0 (nuclear norm minimization).

Omega = logical(Omega);
meanT = sum(T(:).*Omega(:)) / sum(Omega(:));

normT2 = sum(((T(:)-meanT).*Omega(:)).^2);

if(isscalar(epsilon))
    epsilonStop = epsilon;
    epsilonNoise = epsilon;
else
    epsilonStop = epsilon(1);
    epsilonNoise = epsilon(2);
end
if(epsilonNoise==0||~exist('CorruptCols','var')||isempty(CorruptCols)||~any(ismember(CorruptCols,1:size(T,2))))
    NoisyData=false;
    CorruptCols = [];
else
    NoisyData=true;
    CorruptCols = intersect(CorruptCols,1:size(T,2));
    NoCorruptCols = setdiff(1:size(T,2),CorruptCols);
    if(isempty(NoCorruptCols))
        AllCorrupt = true;
        normTCorrupt2 = normT2;
    else
        AllCorrupt = false;
        
        meanTCorrupt = sum(vec((T(:,CorruptCols,:).*Omega(:,CorruptCols,:)))) / sum(vec(Omega(:,CorruptCols,:)));
        normTCorrupt2 = sum(vec(((T(:,CorruptCols,:)-meanTCorrupt).*Omega(:,CorruptCols,:))).^2);
    end
end

if (~exist('X','var')||isempty(X))
    X = T;
    X(~Omega) = meanT;
end
if (~exist('TraceNormToRankParam','var')|| isempty(TraceNormToRankParam))
    TraceNormToRankParam=0;
end
if(~exist('maxRank','var')||isempty(maxRank))
    maxRank=inf;
end

if(TraceNormToRankParam==0)
    legacyThreshold = true;
end

%Check the valid dimensions for tensor unfolding
TensorDim = ndims(T);
if(TensorDim==2)
    TensorDim=1;
    alpha=1;
    ValidDims=1;
else
    ValidDims = 1:TensorDim; ValidDims(alpha(ValidDims)==0)=[];
end

dim = size(T);
M = cell(TensorDim, 1);
Y = M;
rank = zeros(TensorDim,1);
beta = zeros(1,TensorDim);
OmegaPrime=[];

trMult=@(x)x'*x;
for i = ValidDims
    M{i} = X;
    Y{i} = zeros(dim, class(T));
    SV12 = sqrt(svds(trMult(Unfold(X,dim,i)), 2));
    if(legacyThreshold)
        beta(i) = 2 / (SV12(1)+SV12(2));    %initial threshold = 1/beta = mean(SV1+SV2);
        beta(i) = beta(i)*(alpha(i).^2);
    else
        beta(i) = 8 / (SV12(1)+SV12(2)).^2; %initial threshold = sqrt(2/beta) = mean(SV1+SV2);
        beta(i) = beta(i)*alpha(i);
    end
end

Ysum = zeros(dim, class(T));
Msum = zeros(dim, class(T));
errList=[];
fprintf('Iteration:                  ');
for k = 1: maxIter
    %% update M
    Ysum = 0*Ysum;
    Msum = 0*Msum;
    for i = ValidDims
        if(legacyThreshold)
            [M{i}, rank(i), ~] = Pro2TraceNorm(Unfold(X-Y{i}/beta(i), dim, i), alpha(i)/beta(i), TraceNormToRankParam);
        else
            [M{i}, rank(i), ~] = Pro2TraceNorm(Unfold(X-Y{i}/beta(i), dim, i), alpha(i)*sqrt(2/beta(i)), TraceNormToRankParam);
        end
        M{i} = Fold(M{i}, dim, i);
        Ysum = Ysum + Y{i};
        Msum = Msum + beta(i)*M{i};
        fprintf('\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b%5i (rank =%4i)',k,rank(i));
    end
    
    if(any(rank > maxRank))
        break;
    end
    
    %% update X (outside Omega)
    X = (Msum + Ysum) / sum(beta);
    
    % compute the error and check stop criterion
    [errList(end+1),errCorrupt2] = SQErrors(X,T,Omega,CorruptCols);
    errList(end) = errList(end) / normT2;
    if (errList(end) < epsilonStop || k == maxIter )
        X_final = X;
        X_final(Omega) = T(Omega);
        fprintf('\n');
        break;
    end

    %% Update X (inside Omega)
    if(NoisyData) %l2 tolerance:
        % Allow a small error epsilon (in l2 norm) on the known elements of the matrix.
        if(AllCorrupt)
            lambda = max(sqrt( errList(end) / (epsilonNoise) ) - 1, 0);
            X = X.*(1-Omega) + ((X + lambda*T)/(1+lambda)).*Omega;
        else
            errCorrupt2 = errCorrupt2 / normTCorrupt2;
            lambda = max(sqrt( errCorrupt2 / (epsilonNoise) ) - 1, 0);
            
            UpdateXOmega(X,T,Omega,lambda,CorruptCols); %faster with mex.
        end
    else
        X = X.*(1-Omega) + T.*Omega;
    end

    %% Update Y
    for i = ValidDims
        Y{i} = Y{i} + beta(i)*(M{i} - X);
    end
    
    % update beta
    beta  = beta  * betaMult;
end
end