function [frames,imgSize,imgRes] = lf2frames(lf)
%lf2FRAMES Summary of this function goes here
%   Detailed explanation goes here

lfSize = size(lf);
lfSize(end+1:4) = 1;
imgSize = lfSize(1:3);
imgRes = lfSize(4:end);

szDist = num2cell(imgSize);
resDist = arrayfun(@(sz) ones(1,sz),imgRes,'UniformOutput',false);

frames = mat2cell(lf,szDist{:},resDist{:});
frames = squeeze(frames);
end