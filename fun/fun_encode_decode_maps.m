function [nbbits,mvis,mocc] = fun_encode_decode_maps(mapdir,mapname,mvis,mocc)
%FUN_ENCODE_DECODE_MAPS Summary of this function goes here
%   Detailed explanation goes here

mocc(mocc==0) = mvis(mocc==0);
mvis(mvis==0) = mocc(mvis==0);

mvisocc = cat(3,mvis,mvis,mocc);

nbbits = fun_encode_map(mvisocc,mapdir,mapname);
mvisocc = fun_decode_map(mapdir,mapname);

mvis = mvisocc(:,:,1);
mocc = mvisocc(:,:,3);

mocc(mvis==mocc) = 0;
mvis(mvis==mocc) = 0;

end