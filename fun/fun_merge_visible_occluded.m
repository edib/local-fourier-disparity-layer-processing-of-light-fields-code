function [color,mask,dims] = fun_merge_visible_occluded(color,mask,dims,sequential)
%FUN_MERGE_VISIBLE_OCCLUDED Summary of this function goes here
%   Detailed explanation goes here

if ~exist('sequential','var'); sequential = false; end

sed = strel('arbitrary',reshape([1,0,0],1,1,1,1,1,[]));

fun = @(color,mask,dims) fun_merge_aux(color,mask,dims,sed);

if iscell(color)
    numlab = numel(color);
    labels = find(~cellfun(@isempty,color));
    
    if sequential
        for lab = 1:numlab
            if ~any(lab==labels); continue; end
            [color{lab},mask{lab},dims{lab}] = fun(color{lab},mask{lab},dims{lab});
        end
    else
        parfor lab = 1:numlab
            if ~any(lab==labels); continue; end
            [color{lab},mask{lab},dims{lab}] = fun(color{lab},mask{lab},dims{lab});
        end
    end
else
    [color,mask,dims] = fun(color,mask,dims);
end

    function [color,mask,dims] = fun_merge_aux(color,mask,dims,sed)
        mask = mask | false(size(color));
        color(~mask) = nan;
        color = fun_fillmissing(color,sed,1);
        color = color(:,:,:,:,:,1);
        mask = any(mask,6);
        color(~mask) = nan;
        mask = any(mask,3);
        dims = dims(1:5);
    end
end