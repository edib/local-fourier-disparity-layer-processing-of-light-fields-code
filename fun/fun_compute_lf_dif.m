function [lfreccol,lfreclab,lfrecdims] = fun_compute_lf_dif(srreccol,srrecmask,srrecdims,srrefcol,srrefmask)
%FUN_COMPUTE_LF Summary of this function goes here
%   Detailed explanation goes here

%%
srxgvs = cellfun(@(srrecdims) srrecdims{1},srrecdims,'UniformOutput',false);
srygvs = cellfun(@(srrecdims) srrecdims{2},srrecdims,'UniformOutput',false);
srcgv = srrecdims{1}{3};
srugv = srrecdims{1}{4};
srvgv = srrecdims{1}{5};

%%
srxmin = min(cellfun(@min,srxgvs));
srxmax = max(cellfun(@max,srxgvs));
srymin = min(cellfun(@min,srygvs));
srymax = max(cellfun(@max,srygvs));

%%
lfxgv = srxmin:srxmax;
lfygv = srymin:srymax;
lfcgv = srcgv;
lfugv = srugv;
lfvgv = srvgv;
lfrecdims = {lfxgv,lfygv,lfcgv,lfugv,lfvgv};

lfsizecol = cellfun(@numel,{lfxgv,lfygv,srcgv,srugv,srvgv});
lfsizelab = cellfun(@numel,{lfxgv,lfygv,  1  ,srugv,srvgv});

%%
[~,lfxind,~] = cellfun(@(srxgv) intersect(lfxgv,srxgv,'stable'),srxgvs,'UniformOutput',false);
[~,lfyind,~] = cellfun(@(srygv) intersect(lfygv,srygv,'stable'),srygvs,'UniformOutput',false);
[~,lfcind,~] = intersect(lfcgv,srcgv,'stable');
[~,lfuind,~] = intersect(lfugv,srugv,'stable');
[~,lfvind,~] = intersect(lfvgv,srvgv,'stable');

%%
lfreccol = nan(lfsizecol);
lfreclab = nan(lfsizelab);
lfrecdif = inf(lfsizelab);
numlab = numel(srreccol);
numc = numel(srcgv);

for lab = 1:numlab
    srmask = srrefmask{lab} & srrecmask{lab};
    srdif = max(abs(srrefcol{lab}-srreccol{lab}),[],3);
    lfdif = lfrecdif(lfxind{lab},lfyind{lab},1,lfuind,lfvind);
    
    srmask = (srdif<lfdif | isnan(srdif) | isnan(lfdif)) & srmask;
    srmask = any(srmask,3);
    
    srrectemp = lfreclab(lfxind{lab},lfyind{lab},1,lfuind,lfvind);
    srrectemp(srmask) = lab;
    lfreclab(lfxind{lab},lfyind{lab},1,lfuind,lfvind) = srrectemp;
    
    srrectemp = lfrecdif(lfxind{lab},lfyind{lab},1,lfuind,lfvind);
    srrectemp(srmask) = srdif(srmask);
    lfrecdif(lfxind{lab},lfyind{lab},1,lfuind,lfvind) = srrectemp;
    
    srmask = repmat(srmask,1,1,numc);
    
    srrectemp = lfreccol(lfxind{lab},lfyind{lab},lfcind,lfuind,lfvind);
    srrectemp(srmask) = srreccol{lab}(srmask);
    lfreccol(lfxind{lab},lfyind{lab},lfcind,lfuind,lfvind) = srrectemp;    
end

end