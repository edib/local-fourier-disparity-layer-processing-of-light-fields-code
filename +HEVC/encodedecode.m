function [nbBits,peaksnr] = encodedecode(varargin)
%ENCODE Summary of this function goes here
%   Detailed explanation goes here

HEVCDir = HEVC.Dir;
if ispc
    TAppEncoder = fullfile(HEVCDir,'bin','TAppEncoder.exe');
elseif isunix
    TAppEncoder = fullfile(HEVCDir,'bin','TAppEncoderStatic');
elseif ismac
    error('custom HEVC not compiled for macOS');
else
    disp('Platform not supported')
end
defaultCfg  = fullfile(HEVCDir,'cfg','encoder_intra_main_rext.cfg');

p = inputParser; p.KeepUnmatched = true; p.StructExpand = true;

p.addParameter('ConfigFile'         ,defaultCfg,@ischar);
p.addParameter('InputFile'          ,'ref.yuv' ,@ischar);
p.addParameter('BitstreamFile'      ,'bit.hevc',@ischar);
p.addParameter('ReconFile'          ,'rec.yuv' ,@ischar);
p.addParameter('LogFile'            ,'log.rtf' ,@ischar);
p.addParameter('SourceWidth'        ,'512'     ,@ischar);
p.addParameter('SourceHeight'       ,'512'     ,@ischar);
p.addParameter('InputBitDepth'      ,'8'       ,@ischar);
p.addParameter('InternalBitDepth'   ,'8'       ,@ischar);
p.addParameter('InputChromaFormat'  ,'420'     ,@ischar);
p.addParameter('FrameRate'          ,'60'      ,@ischar);
p.addParameter('FrameSkip'          ,'0'       ,@ischar);
p.addParameter('FramesToBeEncoded'  ,'1'       ,@ischar);
p.addParameter('QP'                 ,'30'      ,@ischar);
p.addParameter('WarnUnknowParameter','1'       ,@ischar);
% Add additional default parameters here

p.parse(varargin{:});

perm = 1:numel(p.Parameters);
indconf = find(strcmp('ConfigFile',p.Parameters));
perm(1) = indconf; perm(indconf) = 1;

results = orderfields(p.Results,perm);
unmatched = p.Unmatched;

parameters = fieldnames (results);
values     = struct2cell(results);

extraParameters = fieldnames (unmatched);
extraValues     = struct2cell(unmatched);

ReconFile     = p.Results.ReconFile;
BitstreamFile = p.Results.BitstreamFile;
LogFile       = p.Results.LogFile;

for file={ReconFile,BitstreamFile,LogFile}
    [folder,~,~] = fileparts(file{:});
    if ~(7==exist(folder,'dir'))
        mkdir(folder);
    end
end

argList = paramToArg([parameters;extraParameters],[values;extraValues]);
command = [TAppEncoder,' ',argList];
disp(command)
status = system(command);
if status, error('HEVC Error'); end

[nbBits,peaksnr] = HEVC.parseLog(LogFile);
end

function argList = paramToArg(parameters,values)
indLog  = strcmp(parameters,'LogFile'); ind = indLog;

argLog  = strjoin(['>' ,values(indLog) ]);
argList = cellfun(@(param,val) ['--',param,'="',val,'"'],...
    parameters(~ind),values(~ind),'UniformOutput',false);

argList = [argList;argLog];
argList = strjoin(argList(:));
end