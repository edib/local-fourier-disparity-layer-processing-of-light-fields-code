clear all, close all, clc

%%
LFDir = 'C:\Users\edib\Light Fields\Heidelberg\full_data\test';
name = 'bedroom';

%%
load(fullfile(LFDir,name,'LF.mat'));
LF = LF.LF;

%%
s = size(LF);
colSpace = 'yuv';
subSamp = '400';
subName = '_rec';
precision = 'uint8';

params.FramesToBeEncoded = num2str(1);
params.InputChromaFormat = num2str(subSamp);
params.QP = num2str(0);
params.colSpace = colSpace;
params.subSamp = num2str(subSamp);
params.precision = precision;

%%
[frames,convFramesRef,subFramesRec] = HEVC.encodeLF(LF,name,params);

%%
HEVC.encode(params);

%%
testHEVC = HEVC.HEVC(params);

