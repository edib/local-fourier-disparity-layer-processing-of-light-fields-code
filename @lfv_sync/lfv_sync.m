classdef lfv_sync < handle
    %LFV_SYNC Summary of this class goes here
    %   Detailed explanation goes here
    
    properties(SetObservable,AbortSet)
        lfvs
    end
    
    methods
        function obj = lfv_sync(varargin)
            %LFV_SYNC_2 Construct an instance of this class
            %   Detailed explanation goes here
            
            obj.lfvs = [varargin{:}];
            
            for it = 1:numel(obj.lfvs)
                addlistener(obj.lfvs(it),'cv'  ,'PostSet',@(src,evnt) obj.update(obj.lfvs(it),'cv'  ));
                addlistener(obj.lfvs(it),'dims','PostSet',@(src,evnt) obj.update(obj.lfvs(it),'dims'));
                addlistener(obj.lfvs(it),'clim','PostSet',@(src,evnt) obj.update(obj.lfvs(it),'clim'));
            end
            
            linkaxes([obj.lfvs.ax]);
        end
    end
    
    methods(Hidden)
        function update(obj,lfv,prop)
            %UPDATE Summary of this method goes here
            %   Detailed explanation goes here
            
            ind = ~(obj.lfvs==lfv);
            for it = 1:find(ind)
                if ~ (obj.lfvs(it).(prop) == lfv.(prop))
                    obj.lfvs(it).(prop) = lfv.(prop);
                end
            end
        end
    end
end