%% Input and output directories
% RecDir = '/home/elian/Documents/FDL_GLobal_RGB';
% RecDir = '/media/elian/Thesis/Experiments/FDL_MMSP/FDL_Global_RGB';
% displayName = 'FDL global';
RecDir = '/media/elian/Thesis/Experiments/FDL_MMSP/FDL_Local_RGB';
displayName = 'FDL local';

% LFNames = {'StillLife'};
% LFNames = {'StillLife','Butterfly','Buddha','Greek','Sideboard'};
% LFNames = {'Bench','Bikes','Danger_de_Mort'};

range_1_9 = {'StillLife','Buddha','Greek','Sideboard','Butterfly'};
range_5_13 = {'tarot','lego_knights'};

%% Run algorithm
%% Select subaperture coding order
for OrderName = {'Circular_2','Hierarchical_2'}
    OrderName = OrderName{:};
    
    s = dir(fullfile(RecDir,OrderName));
    s = s([s.isdir]);
    LFNames = {s(3:end).name};

    for LFName = LFNames
        LFName = LFName{:};
        
        %% Load light field
        %list of v indices of the images to load from the files.
        switch LFName
            case range_1_9
                QPList = [40,30,20,15,10,5];
            case range_5_13
                QPList = [40,30,20,15];
            otherwise
                QPList = [40,30,20,15];
        end
        
        %%
        resuDir = fullfile(RecDir,OrderName,LFName);
        
        %% Run algorithm
        try
            fun_plot_bitrate_psnr(resuDir,LFName,QPList,displayName);
        catch
            warning(['Error on lightfield ',LFName]);
        end
    end
end