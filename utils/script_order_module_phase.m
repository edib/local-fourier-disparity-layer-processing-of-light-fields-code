clear all, close all, clc
%%
load('9x9_Circular_Shorter.mat')
%load('9x9_Hierarchical_Shorter.mat')
Order = double(Order);
% Order(Order<4) = 1;
% Order(Order==4) = 2;
%
% Order([3,7],[2,8]) = 2;
% Order([2,8],[3,7]) = 2;

SubOrder = nan(size(Order));
[X,Y] = ndgrid(-4:4);
Phase = -angle((Y+1j*X));
%Module = max(abs(X),abs(Y));
n = 2;
scale = 0.9;
Module = round( abs(X.^n+Y.^n).^(1/n) );

%%
offset = 0;
for it = 1:max(Order(:))
    cur_mod = Module(Order==it);
    cur_phase = Phase(Order==it);
    
    sig = [];
    for sub_it = unique(cur_mod)'
        sub_mod = cur_mod==sub_it;
        sub_phase = cur_phase(sub_mod);
        
        [~,sig_] = sort(sub_phase,'ascend');
        inv_sig = [];
        inv_sig(sig_) = 1:numel(sig_);
        
        sig(sub_mod) = offset + inv_sig;
        offset = offset + nnz(sub_mod);
    end
    
    SubOrder(Order==it) = sig;
end

%%
[~,final_sig] = sort(SubOrder(:),'ascend');
[x,y] = ind2sub([9,9],final_sig');
dx = [diff(x),0];
dy = [diff(y),0];

%%
%close all
figure, hold on
imagesc(SubOrder)
%quiver(x,y,dx,dy,0)
c = jet(numel(dx)-1);
for it = 1:numel(dx)-1
    quiver(x(it),y(it),dx(it),dy(it),0,'Color',c(it,:),'LineWidth',2)
end
hold off

%%
% save('9x9_Circular_Shorter.mat','SubOrder','-append');

