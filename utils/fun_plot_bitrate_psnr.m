function fun_plot_bitrate_psnr(resu_dir,lf_name,qp_list,display_name)
%ENCODEFDL Summary of this function goes here
%   Detailed explanation goes here

tStart = tic;

%% Initialize result arrays
sz  = [1,numel(qp_list)];
bitrate = nan(sz);
psnr_y_mean = nan(sz);
psnr_y_all = nan(sz);

%% Load reference
ref_dir = fullfile(resu_dir,'Reference');
lf_ref = LF.fromSubAps(ref_dir,'ext','png','name',lf_name);

%% Compute quality metrics for each QP value
for qp = qp_list
    qp_ind = qp == qp_list;
    %% Current directories
    rec_dir  = fullfile(resu_dir,['Reconstruction_',num2str(qp)]);
    resu_mat = fullfile(resu_dir,['Results_',num2str(qp),'.mat']);
    m = matfile(resu_mat);
    bitrate(qp_ind) = m.bitrate_all;
    
    %% Prediction of current frame layer using Fourier Disparity Layers
    lf_rec = LF.fromSubAps(rec_dir,'name',lf_name,'ext','png');
    [~,psnr_y_mean(qp_ind),psnr_y_all(qp_ind)] = LF.qualityMetrics(lf_rec,lf_ref);
end

%%
h = figure;
plot(bitrate,psnr_y_mean,'DisplayName',display_name);
ax = gca;
ax.XScale = 'log';

title([lf_name,', mean PSNR_Y']);
xlabel('bitrate (bpp)');
ylabel('PSNR (dB)');
grid on
grid minor
axis tight
legend('show','Location','nw');

saveas(h,fullfile(resu_dir,[lf_name,'_bitrate_psnr_mean']),'fig');
saveas(h,fullfile(resu_dir,[lf_name,'_bitrate_psnr_mean']),'svg');
saveas(h,fullfile(resu_dir,[lf_name,'_bitrate_psnr_mean']),'pdf');
saveas(h,fullfile(resu_dir,[lf_name,'_bitrate_psnr_mean']),'png');

close(h)

%%
h = figure;
plot(bitrate,psnr_y_all,'DisplayName',display_name);
ax = gca;
ax.XScale = 'log';

title([lf_name,', PSNR_Y on all views']);
xlabel('bitrate (bpp)');
ylabel('PSNR (dB)');
grid on
grid minor
axis tight
legend('show','Location','nw');

saveas(h,fullfile(resu_dir,[lf_name,'_bitrate_psnr_all']),'fig');
saveas(h,fullfile(resu_dir,[lf_name,'_bitrate_psnr_all']),'svg');
saveas(h,fullfile(resu_dir,[lf_name,'_bitrate_psnr_all']),'pdf');
saveas(h,fullfile(resu_dir,[lf_name,'_bitrate_psnr_all']),'png');

close(h)

%%
toc(tStart)

end