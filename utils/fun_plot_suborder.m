function fun_plot_suborder(SubOrder)
%FUN_PLOT_SUBORDER Summary of this function goes here
%   Detailed explanation goes here

[~,final_sig] = sort(SubOrder(:),'ascend');
[x,y] = ind2sub([9,9],final_sig');
dx = [diff(x),0];
dy = [diff(y),0];
c = jet(numel(dx)-1);

figure, hold on
imagesc(SubOrder)
for it = 1:numel(dx)-1
    quiver(x(it),y(it),dx(it),dy(it),0,'Color',c(it,:),'LineWidth',2)
end
hold off

end

