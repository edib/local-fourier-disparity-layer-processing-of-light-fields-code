% Author :  Hadi Amirpour 
% email  :  hadi.amirpour@gmail.com
% Copyright(c) EmergIMG,
%              Universidade da Beira Interior
% inputs :  
            %ref   = reference image,
            %rec   = reconstructed image, 
            %n_rgb = number of the bits for RGB inputs, this number should be either 8 or 10.
            %n_yuv = number of bits for YUVs, this number is the same as n_rgb
% outputs:  
            %Y_PSNR 
            %YUV_PSNR 
            %Y_SSIM      

function [Y_PSNR Y_SSIM]=QM_Y(ref,rec,n_rgb,n_yuv)

    %convert rgb to double
    ref_double = double(ref).*(2^n_yuv-1)./(2^n_rgb-1);
    rec_double = double(rec).*(2^n_yuv-1)./(2^n_rgb-1);
    
    
    
    Y1 = ref_double;
    
    Y2 = rec_double;
    


    % Objective metrics
    Y_MSE=immse(Y1,Y2);
    
    Y_PSNR   = 10*log10((2^n_yuv-1)*(2^n_yuv-1)/Y_MSE);
    
    Y_SSIM = ssim(double(Y1)./(2^n_yuv-1),double(Y2)./(2^n_yuv-1));
end