function toGIF(data,filename,varargin)
%TOGIF Summary of this function goes here
%   Detailed explanation goes here

p = inputParser;

p.addOptional('map',gray(256),@isnumeric);
p.addOptional('DelayTime',0.5,@isscalar);

p.parse(varargin{:});
map = p.Results.map;
DelayTime = p.Results.DelayTime;

data = utils.recast(data,'uint8');
frames = LF.toSlices(data);

maps = cellfun(@(f) map,frames,'UniformOutput',false);

ndims_ = cellfun(@ndims,frames,'UniformOutput',false);
assert(isequal(ndims_{:}));
ndim = ndims_{1};

if ndim==3
    frames = cellfun(@(f,map) rgb2ind(f,map,'nodither'),frames,maps,'UniformOutput',false);
end

imwrite(frames{1},maps{1},filename,'gif','LoopCount',Inf,'DelayTime',DelayTime);
for idx = 2:numel(frames)
    imwrite(frames{idx},maps{idx},filename,'gif','WriteMode','append','DelayTime',DelayTime);
end

end

