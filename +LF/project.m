function C = project(LFRef,LFB)
%PROJECT Summary of this function goes here
%   Detailed explanation goes here

LFSize = size(LFRef);
ImgSize = LFSize(1:3);
ImgRes  = LFSize(4:end);

LFRef  = reshape(LFRef,prod(ImgSize),prod(ImgRes));
LFB    = reshape(LFB  ,prod(ImgSize),[]);

C = pinv(LFB)*LFRef;

end