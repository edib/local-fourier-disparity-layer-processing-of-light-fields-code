function [datax,datay] = gradient(data)
%GRADIENT Summary of this function goes here
%   Detailed explanation goes here

frames = LF.toSlices(data);

[framesx,framesy] = cellfun(@gradient,frames,'UniformOutput',false);

datax = LF.fromSlices(framesx);
datay = LF.fromSlices(framesy);
end