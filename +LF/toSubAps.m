function  toSubAps(data,Dir,varargin)
%TOSUBAPS Write lightfield as subapertures
%   toSubAps(data,Dir,varargin)

if iscell(data)
    alphaind = strcmp('alpha',varargin);
    if any(alphaind)
        alpha = varargin{find(alphaind,1,'last')+1};
        alphaind([find(alphaind),find(alphaind,1)+1]) = true;
    else
        alpha = cell(1,numel(data));
    end
    
    varargin = varargin(~alphaind);
    for lab = 1:numel(data)
        LF.toSubAps(data{lab},fullfile(Dir,num2str(lab)),varargin{:},'alpha',alpha{lab});
    end
    
    return;
end

mask = true(size(data,4),size(data,5));
maxValue = [];
if isinteger(data); maxValue = intmax(class(data)); end

% Parse input parameters
p = inputParser; p.KeepUnmatched = true; p.StructExpand = true;
p.addParameter('name'    , ''       , @ischar);
p.addParameter('shift'   , 0        , @isnumeric);
p.addParameter('mask'    , mask     , @islogical);
p.addParameter('ext'     , 'png'    , @ischar);
p.addParameter('map'     , []       , @(x) isempty(x)||isnumeric(x));
p.addParameter('alpha'   , []       , @(x) isempty(x)||isnumeric(x));
p.addParameter('maxValue', maxValue , @(x) isempty(x)||isnumeric(x));
p.addParameter('zero_ind', false    , @islogical);

p.parse(varargin{:});

name     = p.Results.name;
mask     = p.Results.mask;
ext      = p.Results.ext;
map      = p.Results.map;
alpha    = p.Results.alpha;
maxValue = p.Results.maxValue;
zero_ind = p.Results.zero_ind;

% Create folder if it does not exist
if ~startsWith(ext,'.'); ext = ['.',ext]; end
if ~exist(Dir,'dir'); mkdir(Dir); end

% Compute frames
frames = LF.toSlices(data);

if ~isempty(alpha)
    frames_alpha = LF.toSlices(alpha);
end

% Set reference angular indices
p.addParameter('inds_u', 1:size(frames,1), @isnumeric);
p.addParameter('inds_v', 1:size(frames,2), @isnumeric);

p.parse(varargin{:});

inds_u = p.Results.inds_u;
inds_v = p.Results.inds_v;

% Set angular indices to save
p.addParameter('ind_u', inds_u, @isnumeric);
p.addParameter('ind_v', inds_v, @isnumeric);

p.parse(varargin{:});

ind_u = p.Results.ind_u;
ind_v = p.Results.ind_v;

subind_u = ismember(inds_u,ind_u);
subind_v = ismember(inds_v,ind_v);

mask(~subind_u,:) = false;
mask(:,~subind_v) = false;

% Switch to zero-based indexing if necessary
if zero_ind; inds_u = inds_u-1; inds_v = inds_v-1; end

% Create filenames for each frame
[U,V] = ndgrid(inds_u,inds_v);
indices = arrayfun(@(u,v) {num2str(u,'%03d'),num2str(v,'%03d')},U,V,'UniformOutput',false);

if isempty(name)
    filenames = cellfun(@(index) strjoin(      index ,'_'),indices,'UniformOutput',false);
else
    filenames = cellfun(@(index) strjoin([name,index],'_'),indices,'UniformOutput',false);
end

filenames = cellfun(@(filename) fullfile(Dir,[filename,ext]),filenames,'UniformOutput',false);

% Only save frames in angular mask
frames = frames(mask);
if ~isempty(alpha)
    frames_alpha = frames_alpha(mask);
end
filenames = filenames(mask);

% Save frames according to extension
switch ext
    case '.png'
        if ~isempty(map)
            fun_write = @(frame,filename) imwrite(frame,map,filename);
            cellfun(fun_write,frames,filenames);
        elseif ~isempty(alpha)
            fun_write = @(frame,filename,frame_alpha) imwrite(frame,filename,'Alpha',frame_alpha);
            
            cellfun(fun_write,frames,filenames,frames_alpha);
        else
            fun_write = @(frame,filename) imwrite(frame,filename);
            cellfun(fun_write,frames,filenames);
        end
    case '.mat'
        for it = 1:numel(frames)
            disparity = frames{it};
            filename = filenames{it};
            save(filename,'disparity');
        end
    case {'.pgm','.ppm'}
        fun_write = @(frame,filename) imwrite(frame,filename,'MaxValue',maxValue);
        
        cellfun(fun_write,frames,filenames);
    case '.pfm'
        warning('implicit conversion to single in pfmwrite');
        
        fun_write = @(frame,filename) LF.pfmwrite(single(frame),filename);
        cellfun(fun_write,frames,filenames);
    case '.tiff'
        [~,nbits] = utils.precision(class(data));
        tagstruct.ImageLength = size(data,1);
        tagstruct.ImageWidth = size(data,2);
        tagstruct.SamplesPerPixel = size(data,3);
        tagstruct.BitsPerSample = nbits;
        tagstruct.Compression = Tiff.Compression.None;
        tagstruct.SampleFormat = Tiff.SampleFormat.IEEEFP;
        tagstruct.Photometric = Tiff.Photometric.MinIsBlack;
        tagstruct.PlanarConfiguration = Tiff.PlanarConfiguration.Chunky;
        
        fun_write = @(frame,filename) fun_write_tiff(frame,filename,tagstruct);
        cellfun(fun_write,frames,filenames);
    otherwise
        fun_write = @(frame,filename) imwrite(frame,filename);
        cellfun(fun_write,frames,filenames);
end

    function fun_write_tiff(frame,filename,tagstruct)
        t = Tiff(filename,'w');
        t.setTag(tagstruct);
        t.write(frame);
        t.close();
    end
end