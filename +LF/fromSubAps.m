function [data,cmap,alpha] = fromSubAps(Dir,varargin)
%FROMSUBAPS Read and reconstruct lightfield from subapertures
%   data = fromSubAps(Dir,varargin)

% Decide to process current directory or subdirectory
folders = dir(Dir);
files = {folders(~[folders.isdir]).name};
folders = {folders([folders.isdir]).name};
folders = folders(3:end);
labels = cellfun(@str2double,folders);
folders = folders(~isnan(labels));
labels = labels(~isnan(labels));

if isempty(files)
    if ~any(labels)
        error('No files found in directory');
    end
    
    warning('No files found in directory, trying subdirectories');
    
    data = cell(1,numel(folders));
    cmap = cell(1,numel(folders));
    alpha = cell(1,numel(folders));
    
    for lab = labels
        [data{lab},cmap{lab},alpha{lab}] = LF.fromSubAps(...
            fullfile(Dir,folders{lab==labels}),varargin{:});
    end
    
    return;
end

% Parse input parameters
p = inputParser; p.KeepUnmatched = true; p.StructExpand = true;
p.addParameter('name'    , ''    , @ischar);
p.addParameter('ext'     , '.png', @ischar);
p.addParameter('zero_ind', false , @islogical);

p.parse(varargin{:});

name     = p.Results.name;
ext      = p.Results.ext;
zero_ind = p.Results.zero_ind;

if ~startsWith(ext,'.')
    ext = ['.',ext];
end

% List files in directory
listing = dir(fullfile(Dir,['*',ext]));

% Select files corresponding to given name and extension
[~,filenames,~] = cellfun(@fileparts,{listing.name},'UniformOutput',false);

% Extract angular indices u and v from filename
ind_u_v = cellfun(@(filename) ...
    regexp(filename,['^',name,'_?([-+]?\d+)_([-+]?\d+)$'],'tokens'),...
    filenames,'UniformOutput',false);

% Remove empty cells
empty = cellfun(@isempty,ind_u_v);
ind_u_v = ind_u_v(~empty);
filenames = filenames(~empty);

% Separate u and v indices
ind_u_v = cellfun(@(x) str2double(x{:}),ind_u_v,'UniformOutput',false);
[ind_u,ind_v] = cellfun(@(x) deal(x(1),x(2)),ind_u_v);

if zero_ind; ind_u = ind_u+1; ind_v = ind_v+1; end

p.addParameter('inds_u', 1:max(ind_u), @isnumeric);
p.addParameter('inds_v', 1:max(ind_v), @isnumeric);

p.parse(varargin{:});

inds_u = p.Results.inds_u;
inds_v = p.Results.inds_v;

% Set angular indices to save
p.addParameter('ind_u', inds_u, @isnumeric);
p.addParameter('ind_v', inds_v, @isnumeric);

p.parse(varargin{:});

ind_u_ = p.Results.ind_u;
ind_v_ = p.Results.ind_v;

subind_u = ismember(ind_u,ind_u_);
subind_v = ismember(ind_v,ind_v_);

subind_uv = subind_u & subind_v;
shift = [min(inds_u),min(inds_v)]-1;

% Keep indices in subsets
ind_u = num2cell(ind_u(subind_uv));
ind_v = num2cell(ind_v(subind_uv));
filenames = filenames(subind_uv);

% Default output
cmap  = [];
alpha = [];

frames = {};
frames_cmap = {};
frames_alpha = {};

% Read frames and assemble them together according to index
cellfun(@fill_frames,filenames,ind_u,ind_v);

% Fill any missing frame
frames = fill_empty(frames);

% Join frames
data  = LF.fromSlices(frames);

ne = ~cellfun(@isempty,frames_alpha);
if any(ne)
    alpha = LF.fromSlices(frames_alpha);
end

ne = ~cellfun(@isempty,frames_cmap);
if any(ne)
    cmap = frames_cmap(ne);
    cmap = cmap{1};
end

%%
    function fill_frames(filename,u,v)
        fullname = fullfile(Dir,[filename,ext]);
        
        u = u-shift(1);
        v = v-shift(2);
        
        switch ext
            case {'.png'}
                [frame,map,alph] = imread(fullname);
                frames      {u,v} = frame;
                frames_cmap {u,v} = map;
                frames_alpha{u,v} = alph;
            case {'.pfm'}
                frame = LF.pfmread(fullname);
                frames{u,v} = frame;
            case {'.mat'}
                m = matfile(fullname);
                frame = m.disparity;
                frames{u,v} = frame;
            otherwise
                frame = imread(fullname);
                frames{u,v} = frame;
        end
    end

    function frames = fill_empty(frames)
        ind_empty = cellfun(@isempty,frames);
        non_empty_frames = frames(~ind_empty);
        frames_size = size(non_empty_frames{1});
        
        replacement = zeros(frames_size,'like',non_empty_frames{1});
        frames(ind_empty) = deal({replacement});
    end
end