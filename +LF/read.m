function LFRec = read(varargin)
%READ Read lightfield 4D(u,v,x,y) data from yuv file
%   LFRec = read(varargin)

filename = fullfile(pwd,'sequence');

% Create input parser scheme
p = inputParser; p.StructExpand = true; p.KeepUnmatched = true;
p.addParameter('filename', filename, @ischar);

% Parse arguments
p.parse(varargin{:});
%filename = p.Results.filename;

% Recover frames
frames = yuv.read(varargin{:});

% Recover LF
LFRec = LF.fromSlices(frames);

end