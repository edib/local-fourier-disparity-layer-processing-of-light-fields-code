function data = ycbcr2rgb(data,varargin)
%RGB2YCBCR Summary of this function goes here
%   Detailed explanation goes here

[~,~,~,n] = size(data);

for it = 1:n
    data(:,:,:,it) = utils.ycbcr2rgb(data(:,:,:,it),varargin{:});
end

end