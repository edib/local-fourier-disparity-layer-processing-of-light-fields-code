function toSubApsPleno(data,Dir,varargin)
%TOSUBAPSPLENO Summary of this function goes here
%   Detailed explanation goes here

if iscell(data)
    for lab = 1:numel(data)
        LF.toSubApsPleno(data{lab},fullfile(Dir,num2str(lab)),varargin{:});
    end
    
    return;
end

% Set default extension according to number of channels
switch size(data,3)
    case 3
        ext = 'ppm';
    otherwise
        ext = 'pgm';
end

% Allow to use non-default file extension
p = inputParser; p.KeepUnmatched = true; p.StructExpand = true;
p.addParameter('ext', ext, @ischar);
p.parse(varargin{:});
ext = p.Results.ext;

% Set bit size to 10 when using ppm
if strcmp(ext,'ppm'); varargin = [varargin,'maxValue',1023]; end

% Convert to 16-bit unsigned integer when using ppm or pgm
if strcmp(ext,{'ppm','pgm'}); data = utils.recast(data,'uint16'); end

% Reorder dimensions
data = permute(flip(flip(data,4),5),[1,2,3,5,4]);

% Write subapertures with zero-indexing by default
LF.toSubAps(data,Dir,'ext',ext,'zero_ind',true,varargin{:});

end