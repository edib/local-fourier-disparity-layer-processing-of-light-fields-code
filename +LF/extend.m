function Value = extend(Value,se,numIter)
%EXTEND Extend signal using structuring element se numIter times
%   Detailed explanation goes here

for i=1:numIter
        Mask = ~isnan(Value);
        Temp = Value;
        Temp(~Mask)=-Inf;
        Temp = imdilate(Temp,se);
        Temp(isinf(Temp))=NaN;
        Value(~Mask) = Temp(~Mask);
end
end