function [B,C,U,S,V] = factorize(M,k,varargin)
%FACTORIZE Summary of this function goes here
%   Detailed explanation goes here

LFSize = size(M);
numDims = ndims(M);
colDims = [];
rowDims = [];

p = inputParser; p.StructExpand = true; p.KeepUnmatched = true;
p.addParameter('colDims', colDims, @isnumeric);
p.addParameter('rowDims', rowDims, @isnumeric);

p.parse(varargin{:});
colDims = p.Results.colDims;
rowDims = p.Results.rowDims;

if (isempty(colDims)&&isempty(rowDims)); colDims = 1:min(3,numDims); end
if isempty(colDims); colDims = setdiff(1:numDims,rowDims); end
if isempty(rowDims); rowDims = setdiff(1:numDims,colDims); end

colSize = LFSize(colDims);
rowSize = LFSize(rowDims);

M = permute(M,[colDims,rowDims]);
M = reshape(M,prod(colSize),prod(rowSize));

rowSize = [k,1];

[B,C,U,S,V] = utils.factorize(M,k);

B = reshape (B,[colSize,rowSize]);
B = ipermute(B,[colDims,rowDims]);

end