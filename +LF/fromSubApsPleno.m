function data = fromSubApsPleno(Dir,varargin)
%FROMSUBAPSPLENO Summary of this function goes here
%   Detailed explanation goes here

% Allow to use non-default file extension
p = inputParser; p.KeepUnmatched = true; p.StructExpand = true;
p.addParameter('ext', 'ppm', @ischar);
p.parse(varargin{:});
ext = p.Results.ext;

% Read subapertures with zero-indexing by default
data = LF.fromSubAps(Dir,'ext',ext,'zero_ind',true,varargin{:});

% Reorder dimensions
if iscell(data)
    for lab = 1:numel(data)
        data{lab} = permute(flip(flip(data{lab},4),5),[1,2,3,5,4]);
    end
else
    data = permute(flip(flip(data,4),5),[1,2,3,5,4]);
end

end