function [data,mask] = complete(data,varargin)
%COMPLETE Summary of this function goes here
%   Detailed explanation goes here

sz = size(data);
mask = ~isnan(data);
numDims = ndims(data);
colDims = [];
rowDims = [];

p = inputParser; p.StructExpand = true; p.KeepUnmatched = true;
p.addParameter('colDims', colDims, @isnumeric);
p.addParameter('rowDims', rowDims, @isnumeric);
p.addParameter('mask'   , mask   , @islogical);

p.parse(varargin{:});

colDims = p.Results.colDims;
rowDims = p.Results.rowDims;
mask    = p.Results.mask | false(sz);

if (isempty(colDims)&&isempty(rowDims)); colDims = 1:min(3,numDims); end
if isempty(colDims); colDims = setdiff(1:numDims,rowDims); end
if isempty(rowDims); rowDims = setdiff(1:numDims,colDims); end

colSize = sz(colDims);
rowSize = sz(rowDims);

data = permute(data,[colDims,rowDims]);
data = reshape(data,prod(colSize),prod(rowSize));

mask = permute(mask,[colDims,rowDims]);
mask = reshape(mask,prod(colSize),prod(rowSize));

[data,mask] = utils.complete(data,varargin{:},'mask',mask);

data = reshape(data,[colSize,rowSize]);
mask = reshape(mask,[colSize,rowSize]);

data = ipermute(data,[colDims,rowDims]);
mask = ipermute(mask,[colDims,rowDims]);

end