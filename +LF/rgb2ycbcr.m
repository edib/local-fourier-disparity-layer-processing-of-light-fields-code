function data = rgb2ycbcr(data,varargin)
%RGB2YCBCR Summary of this function goes here
%   Detailed explanation goes here

[~,~,~,n] = size(data);

for it = 1:n
    data(:,:,:,it) = utils.rgb2ycbcr(data(:,:,:,it),varargin{:});
end

end