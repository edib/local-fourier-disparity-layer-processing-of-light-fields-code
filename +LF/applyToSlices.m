function data = applyToSlices(data,fun,varargin)
%APPLYTOSLICES Summary of this function goes here
%   Detailed explanation goes here

[~,~,~,n] = size(data);

for it = 1:n
    data(:,:,:,it) = fun(data(:,:,:,it),varargin{:});
end

end