function frames = convert(frames,varargin)
%CONVERT Summary of this function goes here
%   Detailed explanation goes here

[~,bitDepth] = utils.precision(class(frames{1}));

p = inputParser; p.StructExpand = true; p.KeepUnmatched = true;
p.addOptional('outColSpace' , 'ycbcr'     , @ischar);
p.addOptional( 'inColSpace' , 'rgb'       , @ischar);
p.addOptional( 'bitDepth'   , bitDepth , @isnumeric);
p.addOptional( 'fullRange'  , true     , @islogical);

p.parse(varargin{:});
inColSpace  = p.Results.inColSpace;
outColSpace = p.Results.outColSpace;
bitDepth    = p.Results.bitDepth;
fullRange   = p.Results.fullRange;

switch ndims(frames{1})
    case 3
        if (strcmp(inColSpace,'rgb') && strcmp(outColSpace,'ycbcr'))
            frames = cellfun(@(f) utils.rgb2ycbcr(f,bitDepth,fullRange),frames,'UniformOutput',false);
        elseif (strcmp(inColSpace,'ycbcr') && strcmp(outColSpace,'rgb'))
            frames = cellfun(@(f) utils.ycbcr2rgb(f,bitDepth,fullRange),frames,'UniformOutput',false);
        end
    case 2
        warning('Monochrome image, no conversion');
    otherwise
        error('Unexpected number of channels');
end
end