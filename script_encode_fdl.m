addpath(genpath('FDL-Toolbox'));
addpath('utils');
addpath('fun');

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%  Parameters for FDL calibration + construction %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% lambdaCalib = 1;
% lambdaConstruct = .1;
numLayers = 30;

FFTParams.gpuInit        = true;
FFTParams.padType        = 'symmetric';
FFTParams.padSize        = [15,15];
FFTParams.Windowing      = true;
FFTParams.windowProfile  = 'hann';
FFTParams.linearizeInput = false;
FFTParams.gammaOffset    = 0;

%% Input and output directories
RefDir = 'C:\Users\Mikael Le Pendu\Desktop/LightFields_RGB';
RecDir = 'C:\Users\Mikael Le Pendu\Desktop/FDL_Local_RGB';

s = dir(RefDir);
s = s([s.isdir]);
LFNames = {s(3:end).name};

range_1_9 = {'StillLife','Buddha','Greek','Sideboard','Butterfly'};
range_5_13 = {'tarot','lego_knights'};

TestOrders = {'Circular_2','Hierarchical_2'};
%TestOrders = {'Hierarchical_4'};

%% Run algorithm
%% Select subaperture coding order
for OrderName = TestOrders
    OrderName = OrderName{:}
    
    for LFName = LFNames
        LFName = LFName{:}
        
        %% Load light field
        %list of v indices of the images to load from the files.
        switch LFName
            case range_1_9
                uRange = 1:9;
                vRange = 1:9;
                crop = [0 0 0 0];
                QPList = [40,30,20,15,10,5];
            case range_5_13
                uRange = 5:13;
                vRange = 5:13;
                crop = [0 0 0 0];
                QPList = [40,30,20,15];
            otherwise
                uRange = 4:12;
                vRange = 4:12;
                crop = [0 0 0 0];
                QPList = [40,30,20,15];
        end
        
        QPList = nan;%set to nan to skip HEVC encoding (only to tet prediction from original view subset).
        
        LFRef = loadLF(fullfile(RefDir,LFName), LFName, 'png', uRange, vRange, crop);
        LFRef = double(LFRef)/double(intmax(class(LFRef)));
        
%         try
            LFLab = loadLF(fullfile(RefDir,LFName,'Segmentation'), LFName, 'png', uRange, vRange, crop);
%         catch
%             LFLab = [];
%             continue
%         end
        
        %%
        resuDir = fullfile(RecDir,OrderName,LFName);
        
        %%
        nU = numel(uRange);
        nV = numel(vRange);
        
        m = matfile(['./LayerConfigurations/',num2str(nU),'x',num2str(nV),'_',OrderName],'Writable',false);
        Order    = m.Order;
        if(~ismember(who(m),'SubOrder'))
            warning('SubOrder is not defined in configuration file. Using default z-scan SubOrder.')
            SubOrder = reshape(1:nU*nV,size(Order));
        else
            SubOrder = m.SubOrder;
        end
        
        %%
        folder_name = ['FDL_Params_',num2str(numLayers)];
        file_name = [LFName,'_',OrderName,'.mat'];
        m = matfile(fullfile(folder_name,file_name));
        
        lambdaCalib = m.lambdaCalibFull;
        lambdaConstruct = m.lambdaConstructFull;
        
        %% Run algorithm
        fun_encode_fdl(resuDir,LFName,...
            LFRef,uRange,vRange,crop,Order,SubOrder,QPList,...
            FFTParams,numLayers,lambdaCalib,lambdaConstruct,LFLab);
        fun_compute_quality_metrics(resuDir,LFName,QPList);
    end
end